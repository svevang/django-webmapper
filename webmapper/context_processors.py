from django.core.urlresolvers import reverse

from webmapper.media_definitions import WEBMAPPER_MEDIA

def webmapper_items(request):
  context = {}
  context['webmapper_media'] = WEBMAPPER_MEDIA
  return context
