import os
import simplejson

from django.http import HttpResponse
from django.http import Http404
from django.http import HttpResponseServerError
from django.http import HttpResponseRedirect
from django.template import RequestContext, loader
from django.utils.safestring import mark_safe

from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404
from django.template import Context, loader, TemplateDoesNotExist
from django.contrib.sites.models import Site
from django.contrib.gis.maps.google.overlays import GMarker
from django.conf import settings
from django.views.generic.detail import DetailView

from webmapper.models import Map
from webmapper.models import KMLLayer
from webmapper.models import Point
from webmapper.models import MapPane
from webmapper.util import get_setting
from webmapper.util import find_backbone_templates

from webmapper.media_definitions import WEBMAPPER_MEDIA

###############################################################################
# Views
###############################################################################

def map_base_frame(request, map_slug=None, map_id=None, extra_context={},
    template="webmapper/map_embed.html"):

  return map_detail(request, 
                    map_id=map_id,
                    map_slug=map_slug,
                    extra_context=extra_context,
                    template=template)

def map_detail(request, map_slug=None, map_id=None, extra_context={}, template="webmapper/map_detail.html"):

  if map_id:
    wmap = get_object_or_404(Map, pk=map_id);
  else:
    wmap = get_object_or_404(Map, slug=map_slug);

  c = RequestContext(request)
  c.update( {'wmap' : wmap,
             'webmapper_media':WEBMAPPER_MEDIA})
  c.update(extra_context)

  t = loader.get_template(template)
  return HttpResponse(t.render(c))

class MapDetail(DetailView):

  model = Map 
  template_name = 'map/webmapper/map_detail.html'
  queryset = Map.objects.all()

  def get_context_data(self, **kwargs):
    context = super(MapDetail, self).get_context_data(**kwargs)

    context.update( {'wmap' : self.object,
                     'webmapper_media':WEBMAPPER_MEDIA})

    return context


def map_list(request, template="webmapper/map_list.html", extra_context={}):

  MapPermissionStrategy = get_setting('API_MAP_PERM')
  map_permission_strategy = MapPermissionStrategy()

  c = RequestContext(request)
  c.update( {'wmaps': map_permission_strategy.list_resource(request)})
  c.update(extra_context)

  t = loader.get_template(template)
  return HttpResponse(t.render(c))

def map_edit(request, map_slug=None, map_id=None, extra_context={}, template="webmapper/map_edit.html"):

  return map_detail(request, 
                    map_id=map_id,
                    map_slug=map_slug,
                    extra_context=extra_context,
                    template=template)

def map_create(request, extra_context={}, template="webmapper/map_create.html"):
  from webmapper.forms.webmap import MapForm

  if request.method == "GET":
    c = RequestContext(request)
    c.update(extra_context)
    form = MapForm()
  elif request.method == "POST":
    form = MapForm(request.POST)
    if form.is_valid():
      pane = MapPane.objects.create()
      form.cleaned_data.update({'user_id':request.user.id, 'map_pane':pane })
      wmap = form.save()
      wmap.user = request.user
      wmap.save()
      return HttpResponseRedirect(get_setting('MAP_CREATE_REDIRECT_URL')(wmap))
  else:
    raise Http404

  c.update({'map_form':form})
  t = loader.get_template(template)
  return HttpResponse(t.render(c))

def webmapper_constants(request):
  "Return constants used by webmapper"

  t = loader.get_template('webmapper/client/js/constants.js')
  c = Context({
      'STATIC_URL':settings.STATIC_URL,
      'extra_client_state': get_setting('EXTRA_CLIENT_STATE'),
      'templates': simplejson.dumps(get_setting('BACKBONE_TEMPLATES'))
    })
  return HttpResponse(t.render(c), mimetype="application/javascript")

###############################################################################
# Test Views
###############################################################################
import os

from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login

def test_specs(request, spec_base_template_path="webmapper/tests/specs", spec_name=None):
  "log in the user as `test_user` and return specs"

  if 'WEBMAPPER_TESTING_SERVER' not in os.environ:
    return  HttpResponseServerError('Please use the test server for running jasmine specs')

  t = loader.get_template(os.path.join(spec_base_template_path, spec_name))
  c = RequestContext(request)

  # TODO spec context 
  return HttpResponse(t.render(c), mimetype="application/javascript")

def test_runner(request):
  "Need to launch a testserver instance to use this view"

  if 'WEBMAPPER_TESTING_SERVER' not in os.environ:
    return  HttpResponseServerError('Please use the test server for running jasmine specs')

  if not request.user.is_authenticated():
    user = authenticate(username='test_user', password='test')
    login(request, user)

  t = loader.get_template('webmapper/tests/SpecRunner.html')
  c = RequestContext(request)
  c['additional_specs'] = get_setting('ADDITIONAL_SPECS')
  c['webmapper_media'] = WEBMAPPER_MEDIA
  return HttpResponse(t.render(c))
