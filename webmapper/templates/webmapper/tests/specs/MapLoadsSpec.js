{% comment %}
vim: tabstop=4:softtabstop=4:shiftwidth=4:expandtab
{% endcomment %}
{% load url from future %}

describe('a map creation process', function(){

// provides a `wmap` variable populated with a webmapper map
{% include "webmapper/tests/specs/_build_destroy_map_before_each.frag" %}

    it('should be able instantiate maps', function(){

        // Everything after the waitsFor must be queued up in `runs` blocks
        runs(function(){
            expect(wmap.id).toBeTruthy();
        })

    })

    describe('a way to interact with maps that do yet exist', function(){

        var newMap = null;

        beforeEach(function(){
        });

        afterEach(function(){
        });

        it('should be created, fetching some defaults from the server', function(){
            newMap = utils.buildWebmapperInstance({id:'new'});
            window.newMap = newMap;
            waitsFor(function(){return newMap.loading.state() != "pending" });

            var createdDeferred = null

            runs(function(){
                createdDeferred = newMap.create()
            })
            waitsFor(function(){return createdDeferred.state() != "pending" });
            runs(function(){
                expect(createdDeferred.isResolved()).toBeTruthy()
            })
        
            var deletedDeferred = null

            // now delete the created map
            runs(function(){
                deletedDeferred = newMap.destroy()
                window.newMap = newMap
            })

            waitsFor(function(){return deletedDeferred.state() != "pending" });

            runs(function(){
                expect(deletedDeferred.isResolved()).toBeTruthy()
            })

        })
    
    })


});
