{% comment %}
vim: tabstop=4:softtabstop=4:shiftwidth=4:expandtab
{% endcomment %}
{% load url from future %}


describe('How points interact with maps', function(){

// provides a `wmap` variable populated with a webmapper map
{% include "webmapper/tests/specs/_build_destroy_map_before_each.frag" %}
    var point = utils.buildPoint()

    describe('How points interact with google maps', function(){
        it('should be added', function(){
                
            runs(function(){
                wmap.points.add(point)
                wmap.trigger('attach-points', wmap.points)
                expect(wmap).toBeTruthy()
                expect(point.markerView).toBeTruthy()
            })
        
        })
        it('should be removed', function(){
                
            runs(function(){
                wmap.points.remove(point)
                expect(wmap.points.length).toBe(0)
            })
        
        })
    })

    describe('How points interact with openlayers maps', function(){
        it('should be added and removed', function(){
                
            runs(function(){
                wmap.mapPane.changeMapWidget('openlayers')
                wmap.points.add(point)
                wmap.trigger('attach-points', wmap.points)
                expect(wmap).toBeTruthy()
                expect(point.markerView).toBeTruthy()
                window.wmap = wmap
            })
        
        })
    })
    
})

