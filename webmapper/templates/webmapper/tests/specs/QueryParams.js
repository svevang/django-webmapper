{% comment %}
vim: tabstop=4:softtabstop=4:shiftwidth=4:expandtab
{% endcomment %}
{% load url from future %}

describe('a way to pass params along with api calls', function(){

    var wmap = null 
    beforeEach(function(){
        wmap = utils.buildWebmapperInstance();
        waitsFor(function(){return wmap.loading.state() != "pending" });
    });
    afterEach(function(){
        if(wmap != null){
            delPromise = wmap.destroy();
            waitsFor(function(){return delPromise.state() != "pending" });
        }
    });

    it('should be configured when the map is instantiated', function(){

        runs(function(){
            var sqp = wmap.points.serializeQueryParams

            wmap.points.setApiParam('foo', 'gee');
            expect(sqp(wmap.points._apiParams)).toContain('foo=gee');

            wmap.points.setApiParam('foo2', 'gee2');
            expect(sqp(wmap.points._apiParams)).toContain('foo=gee&foo2=gee2');

            wmap.points.setApiParam('foo2', 'baz');
            expect(sqp(wmap.points._apiParams)).toContain('foo=gee&foo2=gee2&foo2=baz');
            
        })
    })

    it('should append registered api parameters to generated urls', function(){
        var wmap = null 
        runs(function(){
             var testTool = webmapper.tools.BaseTool.extend({
                    delegate: function(map){
                        map.points.setApiParam('handler','foo');
                        console.log('delegated!', map.points.setApiParam);
                    }
                });
            wmap = utils.buildWebmapperInstance({tools:[new testTool()]});
        })
        waitsFor(function(){return wmap.loading.state() != "pending" });

        runs(function(){
            expect(wmap.points.url()).toContain('handler=foo');
        
        })

    })

})

