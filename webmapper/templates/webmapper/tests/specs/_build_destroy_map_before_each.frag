    var wmap = null 
    beforeEach(function(){
        wmap = utils.buildWebmapperInstance();
        waitsFor(function(){return wmap.loading.state() != "pending" });
    });
    afterEach(function(){
        if(wmap != null){
            delPromise = wmap.destroy();
            waitsFor(function(){return delPromise.state() != "pending" });
            runs(function(){
              expect(delPromise.isResolved()).toBeTruthy();
            })
        }
    });

