{% comment %}
vim: tabstop=4:softtabstop=4:shiftwidth=4:expandtab
{% endcomment %}
{% load url from future %}

describe('map pane manipulation', function(){

// provides a `wmap` variable populated with a webmapper map
{% include "webmapper/tests/specs/_build_destroy_map_before_each.frag" %}

    var wmap_pane_expectations = function(wmap){
            expect(wmap.id).toBeTruthy();
            expect(wmap.mapPane.view.reflowSize()).toBeUndefined();
            expect(wmap.mapPane.view.paneDiv()).toBeTruthy();
            expect(wmap.mapPane.view.syncMapWidget()).toBeUndefined();
            expect(wmap.mapPane.view.getBounds()).toBeTruthy();
    }

    it('should have standard mapPane attributes', function(){

        // Everything after the waitsFor must be queued up in `runs` blocks
        runs(function(){

            wmap_pane_expectations(wmap);
            window.wmap = wmap;
            wmap.mapPane.changeMapWidget('openlayers');
            wmap_pane_expectations(wmap);

        });

    });

    it('should respond to click events', function(){

        var finished = false;
        var proveClick = function(clickEvent){
            expect(clickEvent.get('lat')).toBeTruthy()
            expect(clickEvent.get('lon')).toBeTruthy()
            finished = true;
        }

        // Everything after the waitsFor must be queued up in `runs` blocks
        runs(function(){

            wmap.mapPane.view.on('click', proveClick)
            // which one is it?, just click on all the el's
            $(wmap.mapPane.view.paneDiv()).find('*').trigger('click')

        })
        waitsFor(function(){ return finished == true; })
        runs(function(){
            expect(finished).toBeTruthy()
        })

        // now do google maps
    
        runs(function(){
            finished = false
            wmap.mapPane.changeMapWidget('google')
            wmap.mapPane.view.on('click', proveClick)
            $(wmap.mapPane.view.paneDiv()).find('*').trigger('click')

        })
        waitsFor(function(){ return finished == true; })
        runs(function(){
            expect(finished).toBeTruthy()
        })

    })

});
