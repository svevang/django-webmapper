<script type="text/template" id="description">
    Title: <h1 class="title<% if(editable){ %> editable<% } %>" name="title" <% if(editable){ %>title="click to edit"<% }%>  ><%= title %></h1>
    Description: <h3 class="description<% if(editable){ %> editable<% } %>" name="description" <% if(editable){ %>title="click to edit"<% }%> ><%= description %></h3>  
    <div class="map-legend"></div>

    <div class="modal-confirmation"> 
      <input type="text" name="new value" />
    </div>

</script>
