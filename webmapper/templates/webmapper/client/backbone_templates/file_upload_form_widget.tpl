<script type="text/template" id="file-upload-form-widget">
<span class="btn btn-success fileinput-button">
      <i class="icon-plus icon-white"></i> 
      <span>Add files...</span>
      <input type="file" name="files[]" <% if(!selectSingle){ %>multiple="multiple"<% } %>>
</span>
</script>

