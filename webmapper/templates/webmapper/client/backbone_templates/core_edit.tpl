<script type="text/template" id="core-edit">
  <table>
    <tr><td>center: </td><td> <%= parseFloat(center.coordinates[1]).toFixed(2) %> <%= parseFloat(center.coordinates[0]).toFixed(2) %></td></tr>
    <tr><td>zoom: </td><td><%= zoom %></td></tr>
    <tr><td colspan="2">
      <input type="radio" name="maptype" value="google" <% if(map_type=='google'){ %><%= "CHECKED" %> <%}%> >Google</input>

      <input type="radio" name="maptype" value="openlayers" <% if(map_type=='openlayers'){ %><%= "CHECKED" %><%}%>>OpenLayers</input>

    </tr>
    <tr><td>layer id: </td><td><%= layerId %></td></tr>


    <tr><td>width: </td><td><input name="width" type="text" size="5" value="<%= width %>"></input></td></tr>
    <tr><td>height: </td><td><input name="height" type="text" size="5" value="<%= height %>"></input></td></tr>
  </table>
</script>
