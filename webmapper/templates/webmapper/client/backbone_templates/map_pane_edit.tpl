<script type="text/template" id="map-pane-edit">
  <div id="<%= mapPaneDomId %>" class="webmapper-mapview-pane" style="width:<% if( width > 860){ %> 100% <% } else{ %><%= css_width %> <% } %>;height:<%= css_height %>">
  </div>
  <h1 class="title"><%= title %></h1>
  <h3 class="description"><%= description %></h3>  
</script>
