{% load url from future %}

window.webmapperConstant = (function(){

  var extraClientState = {
  {% for k,v in extra_client_state.items %}
    {{k}}: '{{v|safe}}' {% if not loop.last %},{% endif %}
  {% endfor %}
  }

  var constants = {
      api_maps_url: '{% url "api_maps" %}',
      api_map_panes_url: '{% url "api_map_panes" %}',
      api_kml_layers_url: '{% url "api_kml_layers" %}',
      api_points_url: '{% url "api_points" %}',
      api_polygons_url: '{% url "api_polygons" %}',
      api_polylines_url: '{% url "api_polylines" %}',
      static_url: '{{STATIC_URL}}'
    
    }
  
  var templateContainer = {
    templates: {}
  }

  _($.parseJSON('{{templates|escapejs}}')).each(function(templateString){

    var templateEl = $(templateString)
    templateContainer.templates[templateEl.attr('id')] = _.template(templateEl.html())

  })

  return _.extend(templateContainer, constants, extraClientState)

})();
