import os

from django.core.management.commands import testserver

from south.management.commands import patch_for_test_db_setup
from webmapper import media_definitions as webmapper_settings
from webmapper.util import get_setting
from django.db import connection, transaction, backend

class Command(testserver.Command):

    def handle(self, *fixture_labels, **options):
        from django.core.management import call_command
        from django.db import connection

        # patch testing for south
        patch_for_test_db_setup()

        verbosity = int(options.get('verbosity', 1))
        #interactive =  options.get('interactive', False)
        interactive =  False
        addrport = options.get('addrport')

        if addrport is '':
          addrport = '127.0.0.1:7999'

        os.environ['WEBMAPPER_TESTING_SERVER'] = 'yes'

        # Create a test database.
        db_name = connection.creation.create_test_db(verbosity=verbosity, autoclobber=not interactive)

        # add the webmapper test fixture 
        # compile a list of fixtures
        fixture_labels = list(fixture_labels) + ['webmapper_test_user.json'] + get_setting('ADDITIONAL_TEST_FIXTURES')

        # Import the fixture data into the test database.
        call_command('loaddata', *fixture_labels, **{'verbosity': verbosity})


        # Hack the default Site object so it points at addrport
        from django.contrib.sites.models import Site
        site = Site.objects.get_current()
        site.domain = addrport
        site.save()
        # flush our connection
        connection.connection.close()

        # Run the development server. auto-reloading causes
        # a strange error -- it causes this handle() method to be called
        # multiple times.
        shutdown_message = '\nServer stopped.\nNote that the test database, %r, has not been deleted. You can explore it on your own.' % db_name
        call_command('runserver', addrport=addrport, shutdown_message=shutdown_message, use_reloader=True, use_ipv6=options['use_ipv6'])
