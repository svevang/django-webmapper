import os
import string
import random

from xml.dom.minidom import parse, parseString
from lxml import etree
from lxml.etree import XMLSyntaxError

from django.contrib.gis.geos import Point as GeosPoint
from django.contrib.gis.geos import LineString as GeosLineString
from django.contrib.gis.geos import Polygon as GeosPolygon
from django.contrib.gis.geos import MultiPolygon as GeosMultiPolygon

from django.contrib.gis.db import models
from django.contrib.auth.models import User as DjangoUser

from django.contrib.contenttypes import generic
from django.contrib.contenttypes.models import ContentType
from django.contrib.sites.models import Site
from django.core.urlresolvers import reverse
from django.conf import settings
from django.template.defaultfilters import slugify
from django.db.models.query import QuerySet, EmptyQuerySet, insert_query, RawQuerySet
from django.contrib.gis import gdal
from django.utils.safestring import mark_safe


MAP_TYPES = [('google','google'),('openlayers','openlayers')]

class WebmapperGeoManager(models.GeoManager):
  pass

class Map(models.Model):

  objects = WebmapperGeoManager()

  title = models.CharField(max_length=128)
  slug = models.CharField(max_length=128)
  description = models.TextField(blank=True, null=True)
  map_pane = models.OneToOneField("MapPane", related_name="map")
  user = models.ForeignKey(DjangoUser, blank=True, null=True)

  # boolean flags that can be used as hooks for ApiPermission models
  # not used in example permissions
  visible = models.BooleanField(default=True)
  public = models.BooleanField(default=False)

  def __unicode__(self):
    return self.slug

  def save(self, *args, **kwargs):

    if self.pk is None and self.map_pane_id is None:
      self.map_pane = MapPane.objects.create()
    self.slug = slugify(self.title)

    return super(Map, self).save(*args, **kwargs)

  def get_absolute_url(self):
    slug = self.slug or "webmapper-map"
    return reverse('map_detail', kwargs={'map_id':self.pk, 'map_slug':slug})

  def get_admin_edit_link(self):
    return reverse('admin:webmapper_map_change', args=[self.pk])

  def clone(self, new_user=None):

    map_values = Map.objects.values().get(id=self.id)
    map_values['id'] = None
    map_values['map_pane_id'] = None
    cloned_map = Map(**map_values)

    pane_values =  MapPane.objects.values().get(id=self.map_pane_id)
    pane_values['id'] = None
    cloned_pane = MapPane.objects.create(**pane_values)
    cloned_map.map_pane = cloned_pane
    cloned_map.user = new_user
    cloned_map.save()


    for model in [Point, PolyLine, Polygon]:
      for p in model.objects.filter(map=self):
        p.id = None
        p.map = cloned_map
        p.save()

    from webmapper.models.options import GoogleMapOptionSet, OpenLayersOptionSet
    for model in [GoogleMapOptionSet, OpenLayersOptionSet]:
      # because the signal handler in __init__.py builds the option sets
      # HACK: delete them
      model.objects.filter(map_pane=cloned_map.map_pane).delete()
      for p in model.objects.values().filter(map_pane=self.map_pane):
        p['id'] = None
        p['map_pane_id'] = None
        p = model(**p)
        p.id = None
        p.map_pane = cloned_map.map_pane
        p.save()


    # TODO, copy the KML files
    #for kml in self.kml_layers.all():

    return Map.objects.get(pk=cloned_map.pk)

  class Meta:
    app_label = "webmapper"


class MapPane(models.Model):

  objects = WebmapperGeoManager()

  center = models.PointField(srid=4326, default="POINT(-103.852729 44.674512)")
  zoom = models.DecimalField(max_digits=2, decimal_places=0, default=3)
  height = models.PositiveIntegerField(default=400, help_text="in pixels")
  width = models.PositiveIntegerField(default=600, help_text="in pixels")
  map_type = models.CharField(max_length=64, choices=MAP_TYPES, default="google")
  kml_layers = models.ManyToManyField("KMLLayer",
                                      related_name="map_panes_kml_layers")
  map_key = models.FileField(upload_to="uploads/webmapper", blank=True, null=True)

  class Meta:
    app_label = "webmapper"

  def __unicode__(self):
    return "%s"%unicode(self.id)

  def get_absolute_url(self):
    return reverse('map_detail', kwargs={'map_slug':self.map.slug}) + "?map_pane=%s"%self.id

  def dom_id(self):
    if not self.id:
      raise
    return "map_pane_%s"%self.id

  def all_kml_layer_ids(self):
    return [x['pk'] for x in self.map.kml_layers.values('pk')]


class KMLLayer(models.Model):

  objects = WebmapperGeoManager()

  title = models.CharField(max_length=128)
  description = models.TextField(blank=True, null=True)

  map = models.ForeignKey(Map, related_name="kml_layers")
  kml = models.FileField(upload_to="uploads/webmapper", max_length=2048, blank=True, null=True)
  kml_url = models.CharField(max_length=2048, blank=True, null=True)
  revision = models.PositiveIntegerField(default=0)

  def save(self, *args, **kwargs):
    self.revision += 1
    add_to_views=False
    if not self.pk:
      add_to_views=True

    res = super(KMLLayer, self).save(*args, **kwargs)

    if add_to_views:
      self.map.map_pane.kml_layers.add(self)

  def __unicode__(self):
    try:
      kml_url = unicode(self.kml.url)
    except ValueError:
      kml_url = unicode(self.kml_url)
    return kml_url

  def get_absolute_kml_url(self, domain_hint=None):
    try:
      kml_url = unicode(self.kml.url)
    except ValueError:
      kml_url = unicode(self.kml_url)
    else:
      if domain_hint:
        kml_url = "http://" + domain_hint + kml_url + "?r=%s"%self.revision
      else:
        current_domain = Site.objects.get(id=settings.SITE_ID).domain
        kml_url = "http://" + current_domain + kml_url + "?r=%s"%self.revision

    return kml_url

  @classmethod
  def create_xml_validation_schema(cls, xsd_path):
   
    with open(xsd_path, 'r') as xsd_fd:
      xmlschema_doc = etree.parse(xsd_fd)
      xmlschema = etree.XMLSchema(xmlschema_doc)
    return xmlschema

  @classmethod
  def ogc_schema(cls):
    ogc_xsd_path = os.path.join(os.path.split(__file__)[0],
                              "../xml_validation/kml/ogc/2.2.0/ogckml22.xsd")
    return KMLLayer.create_xml_validation_schema(goog_xsd_path)

  @classmethod
  def goog_schema(cls):
    goog_xsd_path = os.path.join(os.path.split(__file__)[0],
                              "../xml_validation/kml/google/kml22gx.xsd")
    return KMLLayer.create_xml_validation_schema(goog_xsd_path)

  def validate_kml(self, schema):
    "Takes an etree parsed xsd and validates self.kml file against it." 

    kml_file = self.kml.file
    kml_file.seek(0)
    tree = etree.parse(kml_file)
    return schema.validate(tree)

  class Meta:
    app_label = "webmapper"

  def _generate_placemark_styleid_dict(self, dom):
    # garbage
    style_url_eles = dom.getElementsByTagName('styleUrl')
    pruned_style_url_eles = []
    placemark_styleid_map = {}
    styleid_styleid_map = {}

    for style_ele in style_url_eles:
      curr_styleid = style_ele.childNodes[0].nodeValue.strip('#')

      if style_ele.parentNode.tagName == "Pair":
        pair_ele = style_ele.parentNode
        for key in pair_ele.getElementsByTagName('key'):
          # just use the "normal" style, not anything fancy
          if key.childNodes[0].nodeValue == "normal":
            styleid_styleid_map[pair_ele.parentNode.attributes['id'].value] = curr_styleid
      # can only be an ele that decends from Document, effectivly a Placemark
      # https://code.google.com/apis/kml/documentation/kmlreference.html#document
      else:
        pruned_style_url_eles.append(style_ele)
        parent_placemark = style_ele.parentNode

        placemark_styleid_map[parent_placemark] = curr_styleid

    # the stylemap ele provides a layer of indirection to the actual style id
    # here we flatten that rel
    style_url_eles = pruned_style_url_eles
    for key,value in placemark_styleid_map.items():
      if value in styleid_styleid_map.keys():
        placemark_styleid_map[key] = styleid_styleid_map[value]

    return placemark_styleid_map

  def map_keys_for_layer(self):

    dom = parse(open(self.kml.path,'r'))

    # Style elements determine the color etc
    # https://code.google.com/apis/kml/documentation/kmlreference.html#style
    style_eles = dom.getElementsByTagName('Style')

    # We need a map to look up style elements by their id attr
    styleid_style_map = {}

    for ele in style_eles:
      styleid_style_map[ele.attributes['id'].value] = ele

    placemark_styleid_map  = self._generate_placemark_styleid_dict(dom)

    POINTS = 'point'
    LINE_STRINGS = 'line_string'
    LINEAR_RINGS = 'linear_ring'
    POLYGONS = 'polygon'

    map_keys = {}

    for placemark, styleid  in placemark_styleid_map.items():

      style = styleid_style_map[styleid]

      points = placemark.getElementsByTagName('Point')
      line_strings = placemark.getElementsByTagName('LineString')
      linear_rings = placemark.getElementsByTagName('LinearRing')
      polygons = placemark.getElementsByTagName('Polygon')

      # for now just do polygons, because equiv to linear_rings
      if linear_rings:
        assert polygons
      if polygons:
        assert linear_rings

      if not style in map_keys:
        map_keys[style] = {}

      possible_repr = {}
      if points and not POINTS in map_keys[style]:
        try:
          icon_style = style.getElementsByTagName('IconStyle')[0]
        except IndexError:
          continue
        map_keys[style][POINTS] = icon_style.toxml()
      if line_strings and not LINE_STRINGS in map_keys[style]:
        try:
          line_style = style.getElementsByTagName('LineStyle')[0]
        except IndexError:
          continue
        map_keys[style][LINE_STRINGS] = line_style.toxml()
      if polygons and not POLYGONS in map_keys[style]:
        try:
          poly_style = style.getElementsByTagName('PolyStyle')[0]
        except IndexError:
          continue
        map_keys[style][POLYGONS] = poly_style.toxml()


################################################################################
# POINT

class Point(models.Model):

  objects = WebmapperGeoManager()

  point = models.PointField(srid=4326)
  map = models.ForeignKey(Map, related_name="points")

  def randomize(self):
    self.point = GeosPoint(random.randrange(-180, 180), random.randrange(-90, 90))


  class Meta:
    app_label = "webmapper"


################################################################################
# Polyline

class PolyLine(models.Model):

  objects = WebmapperGeoManager()

  poly_line =  models.LineStringField(srid=4326)
  map = models.ForeignKey(Map, related_name="polylines")

  def randomize(self, segments=5):
    points = []
    for i in xrange(0, segments + 1):
      points.append((random.randrange(-180, 180), random.randrange(-90, 90)))
    self.poly_line = GeosLineString(points)

  class Meta:
    app_label = "webmapper"


################################################################################
# Polygon

class Polygon(models.Model):

  objects = WebmapperGeoManager()

  polygon = models.MultiPolygonField(srid=4326)
  map = models.ForeignKey(Map, related_name="polygons")

  def randomize(self, segments=5):
    import math

    circle_size = 10

    points = []
    center_x = random.randrange(-160, 160)
    center_y = random.randrange(-70, 70)

    step = (2 * math.pi) /float(segments + 1)

    for i in xrange(0, segments + 1):
      points.append((math.cos(i*step) * circle_size + center_x, math.sin(i*step) * circle_size + center_y))
    points.append(points[0])

    self.polygon = GeosMultiPolygon([GeosPolygon(points)])

  class Meta:
    app_label = "webmapper"

################################################################################
# GeoItem

class DjangoModelGeoItemEdge(models.Model):
  ""
  geo_content_type = models.ForeignKey(ContentType, related_name="geo_edges")
  geo_object_id = models.PositiveIntegerField()
  geo_object = generic.GenericForeignKey()

  objects = WebmapperGeoManager()

  content_type = models.ForeignKey(ContentType)
  object_id = models.PositiveIntegerField()
  object = generic.GenericForeignKey()

  class Meta:
    app_label = "webmapper"

