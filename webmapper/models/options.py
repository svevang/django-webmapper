from django.contrib.gis.db import models
from webmapper.models.webmap import MapPane, Map

MAP_TYPES = [ ("terrain","terrain"),
              ("hybrid","hybrid"), 
              ("roadmap","roadmap"),
              ("satellite","satellite") ]

class GoogleMapOptionSet(models.Model):

  "adopting js style attr names for convenience"

  objects = models.GeoManager()

  map_pane = models.OneToOneField(MapPane, related_name="google_options")

  disableDefaultUI = models.BooleanField(default=False)
  disableDoubleClickZoom = models.BooleanField(default=False)
  draggable = models.BooleanField(default=True)
  mapTypeControl = models.BooleanField(default=True)
  mapTypeId = models.CharField(max_length=16, choices=MAP_TYPES, default="terrain")
  noClear = models.BooleanField(default=False)
  scaleControl = models.BooleanField(default=True)
  scrollwheel = models.BooleanField(default=True, help_text="Disabled by default on embedded maps")

  zoom = property(lambda self:self.map_pane.zoom)

    
  class Meta:
    app_label = "webmapper"


class OpenLayersOptionSet(models.Model):
  
  map_pane = models.OneToOneField(MapPane, related_name="openlayers_map_options")
  num_zoom_levels = models.PositiveIntegerField(default=20);
  map_type = models.CharField(max_length=64, default="terrain")

  zoom = property(lambda self:self.map_pane.zoom)
  class Meta:
    app_label = "webmapper"

