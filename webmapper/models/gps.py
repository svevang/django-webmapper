import os
import time

from lxml import etree
from xml.dom.minidom import parse
from dateutil import parser

from django.contrib.gis.gdal import DataSource
from django.contrib.gis.geos import GEOSGeometry
from django.contrib.gis.geos import MultiLineString, LineString
from django.contrib.gis.db import models

from webmapper.models.webmap import WebmapperGeoManager
from webmapper.util.spline import HermitePoint, HermiteSpline
from webmapper.util.coordinate_arithmetic import increment_lon, increment_lat

def create_timestamp_from_datetime(dt):
  seconds = time.mktime(dt.timetuple())
  return seconds + (dt.microsecond / 1000000.0)

class GPXSplineProvider(models.Model):

  _hermite_spline = None

  _left_p = None
  _right_p = None

  @property
  def spline(self):
    if self._hermite_spline is None:
      points = []
      for d in self.gpxpoint_set.values('point','time', 'pk'):
        x,y = d['point'].coords
        del d['point']
        d['time'] = create_timestamp_from_datetime(d['time'])
        points.append(HermitePoint(x,y,d))

      self._hermite_spline = HermiteSpline(points)
    return self._hermite_spline

  def bounds(self):
    if not self._left_p:
      self._left_p =self.gpxpoint_set.all()[0]
    if not self._right_p:
      self._right_p = list(self.gpxpoint_set.all())[-1]
    return self._left_p, self._right_p

  def calculate_heading_at_points(self):
    for p in gpx_segment.gpxpoint_set.all()[1:]:
      p1 = p2
      p2 = p
      x = p2.point.x - p1.point.x
      y = p2.point.y - p1.point.y
      print math.atan2(x, y)%(2*math.pi)
  class Meta:
    abstract = True


class GPX(GPXSplineProvider):

  objects = WebmapperGeoManager()
  gpx_file = models.FileField(upload_to='uploads/gpx')

  _parsed_tree = None
  _parsed_dom = None

  @property
  def dom(self):
    if not self._parsed_dom:
      self._parsed_dom = parse(self.gpx_file.path)
    return self._parsed_dom

  @property
  def tree(self):
    if not self._parsed_tree:
      self._parsed_tree = etree.parse(self.gpx_file.path)
    return self._parsed_tree

  @property
  def tree_root(self):
    return self.tree.getroot()

  def validate_gpx(self):

    gpx_schema = os.path.join(os.path.split(__file__)[0],
                              "../xml_validation/gpx.xsd")
    self.gpx_file.file.seek(0)
    xsd_fd = open(gpx_schema,'r')

    xmlschema_doc = etree.parse(xsd_fd)
    xmlschema = etree.XMLSchema(xmlschema_doc)
    validation_result = xmlschema.validate(self.tree)

    self.gpx_file.file.seek(0)
    xsd_fd.close()
    return validation_result

  def flush_gpx_data(self):
    self.gpxpoint_set.all().delete()
    self.gpxtrack_set.all().delete()
    self.gpxsegment_set.all().delete()

  def time_from_gpx_text(self, text):
    #year, time = "2011-03-11T22:02:29Z".split("T")
    year, time = text.split("T")
    time = time.strip("Z")
    date_string = " ".join([year, time])
    return parser.parse(date_string)

  def create_tracks_and_points(self, flush_old_gpx_data=True):
    "parsing the GPX file, create a tree data-model"

    if flush_old_gpx_data:
      self.flush_gpx_data()

    default_ns = self.tree_root.nsmap[None]
    tag_selector = "{%s}%%s"%default_ns

    for track_ele in self.tree_root.findall(tag_selector%"trk"):
      track_segments = []
      for segment_ele in track_ele.findall(tag_selector%"trkseg"):
        trk_points = []
        for track_point_ele in segment_ele.findall(tag_selector%"trkpt"):
          x,y = track_point_ele.attrib['lon'], track_point_ele.attrib['lat']
          coords_wkt = "POINT(%s %s)"%(x,y)
          dt_time = self.time_from_gpx_text(track_point_ele.find(tag_selector%"time").text)
          gpx_point = GPXPoint(gpx=self, point=coords_wkt, time=dt_time)
          trk_points.append(gpx_point)
        #TODO Need SQL here!
        for p in trk_points:
          p.save()
        track_segments.append(LineString([p.point for p in trk_points]))
      name_ele = track_ele.find(tag_selector%"name")
      gpx_track = GPXTrack.objects.create(name=name_ele.text, 
                                          gpx=self,
                                          track=MultiLineString(track_segments))
      for line_string in track_segments:
        gpx_segment = GPXSegment.objects.create(gpx=self,
                                            gpx_track=gpx_track,
                                            segment=line_string
        )
        point_qs = GPXPoint.objects.filter(gpx=self, point__intersects=gpx_segment.segment)
        point_qs.update(gpx_segment=gpx_segment, gpx_track=gpx_segment.gpx_track)

    #from IPython.Shell import IPShellEmbed
    #ipshell = IPShellEmbed()
    #ipshell() # this call anywhere in your program will start IPython   

  def get_gdal_geoms(self):
    ds = self.get_gdal_datasource()
    return [x.get_geoms() for x in ds]

  class Meta:
    app_label = "webmapper"


class GPXTrack(models.Model):

  name = models.CharField(max_length=1024, blank=True)
  gpx = models.ForeignKey(GPX)
  track = models.MultiLineStringField()

  class Meta:
    app_label = "webmapper"

class GPXSegment(GPXSplineProvider):

  gpx = models.ForeignKey(GPX)
  gpx_track = models.ForeignKey(GPXTrack)
  segment = models.LineStringField()
  objects = models.GeoManager()

  class Meta:
    app_label = "webmapper"

class GPXPoint(models.Model):

  gpx = models.ForeignKey(GPX)

  gpx_track = models.ForeignKey(GPXTrack, blank=True, null=True)
  gpx_segment = models.ForeignKey(GPXSegment, blank=True, null=True)

  time = models.DateTimeField()
  point = models.PointField()
  objects = models.GeoManager()

  @property
  def timestamp(self):
    return create_timestamp_from_datetime(self.time)

  def __unicode__(self):
      return unicode(self.time)

  class Meta:
    ordering = ["time"]
    app_label = "webmapper"

from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic

class GeoPhoto(models.Model):

  point = models.PointField(blank=True, null=True)
  time = models.DateTimeField()
  offset_time = models.DateTimeField(blank=True, null=True)
  gpx = models.ForeignKey(GPX)

  content_type_id = models.PositiveIntegerField()
  object_id = models.PositiveIntegerField()

  photo_field = models.CharField(max_length=1024)

  _mercator_point = None
  _object_cache = None

  @property
  def photo(self):
    attr = getattr(self.obj,self.photo_field)
    if callable(attr):
      photo_field = attr()
    else:
      photo_field = attr
    return photo_field

  @property
  def obj(self):
    if not self._object_cache:
      if self.content_type_id and self.object_id:
        ct = ContentType.objects.get(id=self.content_type_id)
        self._object_cache = ct.model_class().objects.get(pk=self.object_id)
      else:
        return None
    return self._object_cache

  @property
  def timestamp(self):
    return create_timestamp_from_datetime(self.time)

  @property
  def google_mercator(self):
    if self.point is None:
      return None
    if self._mercator_point is None:
      self._mercator_point = self.point.clone()
      self._mercator_point.transform(900913)
    return self._mercator_point

  def discover_date_from_exif(self):
    from webmapper.util import EXIF

    f = getattr(self.obj,self.photo_field)
    f.seek(0)
    date_string = EXIF.process_file(f)['Image DateTime'].values
    year_section, minute_section = date_string.split()
    year_section = year_section.replace(':','-')
    recombined = " ".join([year_section, minute_section])

    res = parser.parse(recombined)
    return res

  def save(self, *args, **kwargs):
    if not self.time:
      self.time = self.discover_date_from_exif()
    return super(GeoPhoto, self).save(*args, **kwargs)

  class Meta:
    ordering = ["time"]
    app_label = "webmapper"

class AerialBoundary(models.Model):

  bounds = models.PolygonField()
  geophoto = models.OneToOneField(GeoPhoto)


  @property
  def south(self):
    return self.bounds.extent[1]

  @property
  def west(self):
    return self.bounds.extent[0]

  @property
  def north(self):
    return self.bounds.extent[3]

  @property
  def east(self):
    return self.bounds.extent[2]

  class Meta:
    app_label = "webmapper"


