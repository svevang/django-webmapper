from django.db.models.signals import post_save
from django.dispatch import receiver

from webmapper.models.webmap import (Map,
                                        MapPane,
                                        KMLLayer,
                                        Point,
                                        PolyLine,
                                        Polygon,
                                        DjangoModelGeoItemEdge,
                                        )

from webmapper.models.options import GoogleMapOptionSet
from webmapper.models.options import OpenLayersOptionSet

from webmapper.models.gps import GPX, GPXPoint, GPXTrack, GPXSegment, GeoPhoto, AerialBoundary


@receiver(post_save, sender=MapPane)
def build_map_pane_attrs(sender, **kwargs):
  if kwargs['created']:
    map_pane = kwargs['instance']
    GoogleMapOptionSet.objects.create(map_pane=map_pane)
    OpenLayersOptionSet.objects.create(map_pane=map_pane)

