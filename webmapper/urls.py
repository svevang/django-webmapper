from django.conf.urls.defaults import *
from django.conf import settings

urlpatterns = patterns('webmapper.views',

     #url(r'^webmapper/$', 'webmapper.views.webmapper', name='webmapper'),

     ##########################################################################
     # DJANGO PISTON
     ##########################################################################
     (r'^api/', include('webmapper.api.urls')),

     ##########################################################################
     # Maps
     ##########################################################################

     url(r'^maps/new/$', 'map_create', name='map_create'),
     url(r'^maps/(?P<map_id>[-\d]+)/(?P<map_slug>[-\w]+)/$', 'map_detail', name='map_detail'),
     url(r'^maps/(?P<map_id>[-\d]+)/(?P<map_slug>[-\w]+)/embed/$', 'map_base_frame', name='map_base_frame'),
     url(r'^maps/(?P<map_id>[-\d]+)/(?P<map_slug>[-\w]+)/edit/$', 'map_edit', name='map_edit'),

     # for convenience include slugless urls
     url(r'^maps/(?P<map_id>[-\d]+)/$', 'map_detail', name='map_detail_slugless'),
     url(r'^maps/(?P<map_id>[-\d]+)/edit/$', 'map_edit', name='map_edit_slugless'),

     
     # Legacy
     # url(r'^maps/(?P<map_slug>[-\w]+)/$', 'webmapper.views.map_detail', name='map_detail'),
     # url(r'^maps/(?P<map_slug>[-\w]+)/embed/$', 'webmapper.views.map_base_frame', name='map_base_frame'),


     url(r'^maps/$', 'map_list', name='map_list'),
     #url(r'^maps/(?P<map_pane_id>.+)/$', 'webmapper.views.map_detail', name='map_pane_detail'),

     url(r'^client/js/webmapper_constants.js$', 'webmapper_constants', name='webmapper_constants'),
)

if settings.DEBUG:
  urlpatterns += patterns('webmapper.views',
       # tests
       url(r'^tests/runner/$', 'test_runner', name='test_runner'),
       url(r'^tests/specs/(?P<spec_name>[\.\w]+)$', 'test_specs', name='test_specs'),
  )



