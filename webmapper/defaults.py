from webmapper.util import get_class
from webmapper.util import webmapper_static_path

from django.utils.functional import lazy
from django.core.urlresolvers import reverse

lazy_reverse = lazy(reverse, unicode)

BASE_TEMPLATE = 'webmapper/webmapper_base.html'
JQUERY_URL = webmapper_static_path('lib/jquery-1.7.2.js')
UPLOAD_PATH = 'uploads/webmapper'

API_AUTH = 'webmapper.api.auth.UserAuth'
API_MAP_PERM = 'webmapper.api.auth.MapPermissions'
API_MAP_PANE_PERM = 'webmapper.api.auth.MapPanePermissions'
API_KML_LAYER_PERM = 'webmapper.api.auth.KMLLayerPermissions'
API_POINT_PERM = 'webmapper.api.auth.PointPermissions'
API_POLYGON_PERM = 'webmapper.api.auth.PolygonPermissions'
API_POLYLINE_PERM = 'webmapper.api.auth.PolyLinePermissions'

MAP_CREATE_REDIRECT_URL = lambda wmap: reverse('map_edit',kwargs={'map_id':wmap.id, 'map_slug': wmap.slug})

# a dictionary mapping {js_url : [list_of_js_deps]}
from webmapper.tests import ADDITIONAL_SPECS

# a list of fixtures paths relative to the fixture search path
from webmapper.tests import ADDITIONAL_TEST_FIXTURES

# key->value store passed in the client-side `webmapperConstants` construct
EXTRA_CLIENT_STATE = {}

def _get_webmapper_templates():
  import os
  from webmapper.util import find_backbone_templates

  # search the webmapper backbone template folder for *.tpl
  app_path = os.path.split(__file__)[0]
  webmapper_template_base = os.path.join(app_path, 'templates/')

  webmapper_backbone_template_base = os.path.join(app_path, 'templates/webmapper/client/backbone_templates/')

  return find_backbone_templates(webmapper_backbone_template_base)

# a list of backbone templates,
# encoded as script tags of type text/template with id attr set
BACKBONE_TEMPLATES = _get_webmapper_templates()
