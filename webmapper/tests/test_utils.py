from webmapper.models import Map
from webmapper.models import MapPane
from webmapper.models import KMLLayer
from webmapper.models import Point
from webmapper.models import PolyLine
from webmapper.models import Polygon


def build_map(count=1):

  maps = []
  for i in xrange(count):
    m = Map.objects.create(title="{0}".format(count))

    maps.append(m)

  return maps


def populate_geo_attrs(wmap, attr_count=5):
  # build random geo attrs

  for count in xrange(0, attr_count):
    for model in [Point, PolyLine, Polygon]:
      p = model(map=wmap)
      p.randomize()
      p.save()
 
    

