import unittest
from django.test.client import Client
from webmapper.media_definitions import WEBMAPPER_MEDIA
from webmapper.tests.test_cases import WebmapperTestCase

class TestSequenceFunctions(WebmapperTestCase):

  def setUp(self):
    pass

  def tearDown(self):
    pass

  def test_assets(self):
    c = Client()
    for js_ref in WEBMAPPER_MEDIA['js']:
      if '/client/js/webmapper' in str(js_ref):
        response = c.get(js_ref) 
        self.assertTrue(response.status_code == 200)

