import unittest
import logging
import simplejson
import django

from webmapper.models import Map, MapPane
from django.test.client import Client

from decimal import Decimal
from django.core.urlresolvers import reverse
from webmapper.tests.test_utils import build_map
from webmapper.tests.test_utils import populate_geo_attrs
from webmapper.tests.test_cases import WebmapperTestCase

logger = logging.getLogger(__file__)

class ApiTest(WebmapperTestCase):

  fixtures = ['webmapper_test_user.json']

  def setUp(self):
    self.map = build_map()[0]
    populate_geo_attrs(self.map)
    self.client = Client()
    self.client.login(username='test_user', password='test')
    from django.contrib.auth.models import User
    u = User.objects.get(username="test_user")
    self.wmap = Map.objects.create(user=u)


  def tearDown(self):
    pass


class MapApi(ApiTest):

  def test_get_maps(self):
    simplejson.loads(self.client.get(reverse('api_maps')).content)
  
  def test_get_new_map(self):
    simplejson.loads(self.client.get(reverse('api_map', kwargs={"map_id":"new"})).content)

  def test_post_maps(self):
    # with no body
    resp = self.client.post(reverse('api_maps'))#, '{}', content_type="application/json")
    assert(resp.content)
    print resp
    map_dict = simplejson.loads(resp.content)
    Map.objects.get(id=map_dict['id'])

################################################################################
# Geo apis
################################################################################

class GeoApiTest(object):

  def test_get_list(self):
    simplejson.loads(self.client.get("{0}?map_id={1}".format(reverse('api_{0}s'.format(self.api)), self.wmap.id)).content)

class MapPanesApi(ApiTest, GeoApiTest):

  api = "map_pane"


class PointsApi(ApiTest, GeoApiTest):

  api = "point"

  def test_post_points(self):
    point_json = simplejson.dumps({'geojson':{'Type':'point', 'coordinates':[1,2]}} )
    resp = self.client.post("{0}?map_id={1}".format(reverse('api_points'),self.wmap.id), 
                            point_json, 
                            content_type="application/json")
    assert(resp.content)
    map_dict = simplejson.loads(resp.content)


class KmlLayersApi(GeoApiTest, ApiTest):

  api = "kml_layer"

class PolygonsApi(ApiTest, GeoApiTest):

  api = "polygon"

class PolylinesApi(ApiTest, GeoApiTest):

  api = "polyline"

