import unittest
from webmapper.models import Map, MapPane
from decimal import Decimal

class TestSequenceFunctions(unittest.TestCase):

  def setUp(self):
    self.m = Map.objects.create(title="Foo")

    self.m.map_pane = MapPane.objects.create()
    self.m.save()

    self.m.map_pane.zoom = Decimal(15)
    self.m.map_pane.save()

  def tearDown(self):
    pass

  def test_clone(self):
    m2 = self.m.clone()
    self.assertTrue(m2.title == "Foo") 
    self.assertTrue(m2.map_pane.zoom == Decimal(15)) 

