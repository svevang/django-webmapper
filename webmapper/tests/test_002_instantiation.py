import unittest
from webmapper.models import Map, MapPane
from webmapper.tests.test_utils import build_map
from webmapper.tests.test_utils import populate_geo_attrs

class TestSequenceFunctions(unittest.TestCase):

  def setUp(self):
    pass

  def tearDown(self):
    pass

  def test_instantiation(self):
    m = build_map()[0]

    # our option sets should have auto generated
    self.assertTrue(m.map_pane.google_options.pk)
    self.assertTrue(m.map_pane.openlayers_map_options.pk)

  def test_geo_attrs(self):
    m = build_map()[0]
    populate_geo_attrs(m)



