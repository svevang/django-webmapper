#!/usr/bin/env python

# Parameterized splines, points interpolated with Hermite polynomials
# Each interval of interpolation is two points with tangents
#
# Monotone Piecewise Cubic Interpolation
# F. N. Fritsch and R. E. Carlson
# modified to use finite difference
#

import warnings
from random import random;
from math import *

# The first 4 hermite polynomials (3rd degree polynomials)

HERMITE_BASIS = [[2,-3,0,1], [1,-2,1,0], [-2, 3, 0, 0], [1,-1,0,0]]

def machine_epsilon(func=float):
    machine_epsilon = func(1)
    while func(1)+func(machine_epsilon) != func(1):
        machine_epsilon_last = machine_epsilon
        machine_epsilon = func(machine_epsilon) / func(2)
    return machine_epsilon_last

class FunctionWarning(Warning):
  pass

class HermiteSplineException(Exception):
  pass

class HermitePoint(object):

  x = None
  y = None
  slope = None
  secant = None
  alpha = None
  beta = None
  prev = None
  next = None


  def __init__(self, x, y, parameters={}):

    self.x = x
    self.y = y
    self.prev = None
    self.next = None
    self.parameters = parameters
    self.parameters.update({'x':x,'y':y})

  def __str__(self):
    return str((self.x, self.y, self.slope))

  def __unicode__(self):
    return unicode(self.__str__())

  def __repr__(self):
    return "<HermitePoint:%s>"%self.__unicode__()

class HermiteSpline(object):

  points = None
  hint = None

  def __init__(self, incoming_points=None):
  
    assert len(incoming_points) > 4

    self.points = incoming_points
    self.link_list()
    self.assign_secants_and_tangents()
    self.build_sorted_parameter_lookup()

  def link_list(self):

    i = 0
    while i < len(self.points):
      point = self.points[i]
      if i == 0:
        point.next = self.points[1]
      elif i == len(self.points) - 1:
        point.prev = self.points[-2]
      else:
        self.points[i].prev = self.points[i-1]
        self.points[i].next = self.points[i+1]
      i = i + 1

  def assign_secants_and_tangents(self):

    i = 0
    while i < len(self.points):
      point = self.points[i]

      if point.secant:
        i += 1
        continue
      if point.next:
        if point.x == point.next.x:
          point.secant = 0
          point.next.secant = 0
          i += 1
          continue
      if i == 0:
        point.secant = self.calc_secant(self.points[i], self.points[i+1])
      elif i == len(self.points) - 1:
        point.secant = 0 
      else:
        self.points[i].secant = self.calc_secant(self.points[i], self.points[i+1])
      i = i + 1

    i = 0
    while i < len(self.points):

      point = self.points[i]
      if point.next:
        if point.x == point.next.x:
          point.slope = 0
          i += 1
          continue

      if i == 0:
        point.slope = point.secant
      elif i == len(self.points) - 1:
        point.slope = point.prev.secant
      else:
        point.slope = (point.prev.secant + point.secant) / 2.0
        #print "point %s, slope %s, secant %s,"%(point, point.slope, point.secant)

        if not point.secant == 0:
          point.alpha = point.slope / point.secant
        #else:
        #  raise Exception

        #delta = (next.y - this.y) / float(next.x - this.x)

        #if not delta == 0:
        #  alpha = this.slope / delta
        #  beta = next.slope / delta
        #  hyp = sqrt( pow(alpha,2) + pow(beta,2) )
        #  # again, ??
        #  if hyp > upper_bound:
        #    self.points[i].slope = (upper_bound / hyp)
      i = i + 1    
        
  def calc_secant(self, p1, p2):
    try:
      return (p2.y - p1.y)/(p2.x - p1.x)
    except ZeroDivisionError:
      raise ZeroDivisionError('in calc secant')


  def check_incrementer(self, parameter_tuple, increment, check_right_only=True):
    param_label, param_value = parameter_tuple

    left, right = self.parameter_bounds(param_label)
    h = right - left

    if param_value > right or param_value < left:
      raise HermiteSplineException('Outside of interpolation boundary')
    if increment > h:
      raise HermiteSplineException('Decrease increment, parameter interval is too small')
    if (param_value + increment) > right:
      overshoot = (param_value + increment) - right
      param_value = param_value - overshoot
    if not check_right_only:
      if (param_value - increment) < left:
        overshoot = left - (param_value - increment)  
        param_value = param_value + overshoot
    return param_label, param_value

  def get_small_increment_at_point():
    pass

  def atan2(self, parameter_tuple, increment=0.000001):
    'calculate atan2 at some point on the spline'

    # unpack the parameter
    # This ends up being something like 
    # "time", 1300993402.0
    param_label, param_value = self.check_incrementer(parameter_tuple, increment)

    # print "interp these values %s %s"%(param_label, param_value)
    # find the current point on the spline and a point just near it
    p0 = self.f((param_label, param_value))
    p1 = self.f((param_label, param_value + increment))
    from django.contrib.gis.geos import Point

    geos_point0 = Point(*p0, srid=4326)
    geos_point1 = Point(*p1, srid=4326)
    geos_point0.transform(900913) # google mercator
    geos_point1.transform(900913) # google mercator
    x0, y0 = geos_point0.coords
    x1, y1 = geos_point1.coords

    return atan2(y1 - y0, x1 - x0)

  def derivative(self, of_label, with_respect_tuple, increment=0.0001):
    "get the derivative *of* _with respect to_"

    #wrt_label, wrt_value = self.check_incrementer(with_respect_tuple, increment, False)
    wrt_label, wrt_value = with_respect_tuple

    #print "\n\ntaking deriv:"
    left, right = self.parameter_bounds(wrt_label)
    #print "BOUNDS %s %s"%(left, right)
  
    if wrt_value - increment < left:
      x0, y0 = self.f((wrt_label, wrt_value))
    else:
      x0, y0 = self.f((wrt_label, wrt_value - increment))
      
    if wrt_value + increment > right:
      x1, y1 = self.f((wrt_label, wrt_value))
    else:
      x1, y1 = self.f((wrt_label, wrt_value + increment))

    #print "x0 %s y0 %s"%(x0,y0)
    #print "x1 %s y1 %s"%(x1,y1)
    
    if of_label == "x":
      of_value0 = x0
      of_value1 = x1
    else:
      of_value0 = y0
      of_value1 = y1

    #print "of_value0 %s of_value1 %s"%(type(of_value0), type(of_value1))
    #print "print difference %s"%str(of_value1-of_value0)

    return (of_value1 - of_value0) / (2*increment)

  def second_derivative(self, of_label, with_respect_tuple, increment=0.0001):

    #wrt_label, wrt_value = self.check_incrementer(with_respect_tuple, increment, False)
    wrt_label, wrt_value = with_respect_tuple

    #print "\n\ntaking deriv:"
    left, right = self.parameter_bounds(wrt_label)
    #print "BOUNDS %s %s"%(left, right)
  
    if wrt_value - increment < left:
      x0, y0 = self.f((wrt_label, wrt_value))
    else:
      x0, y0 = self.f((wrt_label, wrt_value - increment))
      
    if wrt_value + increment > right:
      x1, y1 = self.f((wrt_label, wrt_value))
    else:
      x1, y1 = self.f((wrt_label, wrt_value + increment))

    x,y = self.f((wrt_label, wrt_value))

    #print "x0 %s y0 %s"%(x0,y0)
    #print "x1 %s y1 %s"%(x1,y1)
    
    if of_label == "x":
      of_value = x
      of_value0 = x0
      of_value1 = x1
    else:
      of_value = y
      of_value0 = y0
      of_value1 = y1

    #print "of_value0 %s of_value1 %s"%(type(of_value0), type(of_value1))
    #print "print difference %s"%str(of_value1-of_value0)

    return (of_value0 - 2*of_value + of_value1) / (increment**2)

  def add_point(self, point):
    # TODO
    pass

  def build_sorted_parameter_lookup(self):
    
    self.parameters_points = {}
    for point in self.points:
      for param_label, param_value in point.parameters.items():
        if param_label in self.parameters_points:
          self.parameters_points[param_label].append(point)
        else:
          self.parameters_points[param_label] = [point]

    for param_label, point_list in self.parameters_points.items():
      point_list.sort(key=lambda point:point.parameters[param_label])

  # Returns the left hand of the interval containing the value.
  # Logorithmic runtime using successive halvings.
  # Use of 'hint' (spatial/temporal locality) allows for constant lookup
  def find_interval_containing_value(self, parameter_tuple):
    """
      Returns the point on the left side of the interval 
      values increase to the right
    """
  
    label, value = parameter_tuple
    point_list = self.parameters_points[label]

    num_points = len(point_list)

    if value < point_list[0].parameters[label] or \
        value > point_list[len(point_list) - 1].parameters[label]:
      raise HermiteSplineException('Outside of interpolation boundary')
 
    if self.hint:
      sel = self.hint
    else:
      sel = int(num_points * random())

    #print "initial position selection: %d" % sel

    left = 0
    right = len(point_list)

    while True:
      #print sel, point_list[sel], point_list[sel].parameters

      if value > point_list[sel].parameters[label]:

        if value < point_list[sel + 1].parameters[label]:
          # Success!
          self.hint = sel
          return self.points[sel]
        else:
          left = sel
          sel = int(floor((right - left) / 2.0) + left)
          #print "moving right, new selection %d" % sel

      elif value < point_list[sel].parameters[label]:

        if value > point_list[sel - 1].parameters[label]:
          # Success!
          self.hint = sel - 1
          return point_list[sel - 1]
        else:
          # increment down
          right = sel
          sel = int(floor((right - left) / 2.0) + left)
          #print "moving left, new selection %d" % sel

      elif value == point_list[sel].parameters[label]:
        if sel == len(point_list) - 1:
          self.hint = sel - 1
          return point_list[sel - 1]
        else:
          self.hint = sel
          return point_list[sel]

  def parameter_bounds(self, param_label):

    if param_label in ['x','y']:
      msg = """\
      The coordinates of spline knots don't necessarily denote a function\
      """ 
      warning.warn(msg, FunctionWarning)

    point_list = self.parameters_points[param_label]
    if len(point_list) < 2:
      msg = 'There are less than 2 parameterized points for %'%param_lable
      raise HermiteSplineException(msg)

    left = point_list[0].parameters[param_label]
    right = point_list[-1].parameters[param_label]
    return left, right

  def f(self, parameter_tuple):

    param_label, param_value = parameter_tuple

    p0 = self.find_interval_containing_value(parameter_tuple)
    p1 = p0.next
    x_eval = self.get_x_for_param((param_label, param_value), p0, p1)
    return x_eval, self.interpolate(x_eval, p0, p1) 
    
  def get_x_for_param(self, parameter_tuple, p0, p1):

    param_label, param_value = parameter_tuple

    param_h = fabs(p1.parameters[param_label] - p0.parameters[param_label])
    param_scale = (param_value - p0.parameters[param_label]) / param_h

    val = ((param_value - p0.parameters[param_label])*p1.x + (p1.parameters[param_label] - param_value)*p0.x)/(p1.parameters[param_label]-p0.parameters[param_label])

    return val

    #print param_scale
    #print "range %s"%fabs(p1.x-p0.x)
    x_val = p1.x - p0.x * param_scale
    #print "p0:%s, p1:%s"%(p0.parameters, p1.parameters)
    #print "x_val%s"%x_val
    return x_val
    if p1.x < p0.x:
      return p0.x - x_val
    else:
      return p1.x - x_val

        
  def interpolate(self, eval_x, p0, p1):

    h = (p1.x - p0.x)
    if h == 0:
      return p0.y
    eval_x = (eval_x - p0.x) / h
    #print "interpolating %s %s %s"%(eval_x,p0,p1)
    return self.eval_hermite_poly(HERMITE_BASIS[0], eval_x ) * p0.y + \
           self.eval_hermite_poly(HERMITE_BASIS[1], eval_x ) * h * p0.slope + \
           self.eval_hermite_poly(HERMITE_BASIS[2], eval_x ) * p1.y + \
           self.eval_hermite_poly(HERMITE_BASIS[3], eval_x ) * h * p1.slope

  def eval_hermite_poly(self, poly, x):

    exponant = len(poly) - 1
    accum = 0
    for coef in poly:
      # the coeffecients are arranged highest power
      # to the lowest, so we start with i=the highest power
      # and work down to zero
      accum = accum + (coef * pow(x, exponant))
      exponant -= 1
    
    return accum


def main():

  point_list = []

  gen = lambda:(random() * 2) - 1
  def mirror(i, center):
    if i > center:
      return center - (i-center)
    else:
      return i

    return (tri(i) + peak) * scale

  x_walker = 0
  y_walker = 0

  x_min = -10
  x_max = 10
  bearing = pi/4
#  for i in range(10):
#
#    bearing += gen()*0.001
#
#    point_list.append(HermitePoint(x=sin(bearing)*random()*0.01, 
#                                   y=cos(bearing)*random()*0.01, 
#                                   parameters={'time':i})) 

#  step = 1
#  count = 0
#  i = 0
#  f = open("/tmp/working",'r')
#  line = f.readline()
#  while line:
#    x,y,time = [float(x) for x in line.split()]
#    line = f.readline()

  point_list.append(HermitePoint(x=0, 
                                 y=-1, 
                                 parameters={'time':1})) 
  point_list.append(HermitePoint(x=0.5, 
                                 y=-0.5, 
                                 parameters={'time':2})) 

  point_list.append(HermitePoint(x=1, 
                                 y=0, 
                                 parameters={'time':3})) 
  point_list.append(HermitePoint(x=0.5, 
                                 y=0.5, 
                                 parameters={'time':4})) 

  point_list.append(HermitePoint(x=0, 
                               y=1, 
                               parameters={'time':5})) 
  point_list.append(HermitePoint(x=-1, 
                                 y=1, 
                                 parameters={'time':6})) 

    

  s1 = HermiteSpline(point_list);

  left = s1.points[0].parameters['time']
  right = s1.points[-1].parameters['time']

#  while True:
#    pos = (right - left) * random() + left
#    print "generating random t: %g " % pos
#    i = s.find_interval_containing_value(pos)
#    assert pos > s.points[i].x and pos < s.points[i + 1].x;

  step_size = (0.2)

  steps = right - left / step_size

  pos = left

# print pos, right
  out = []
  while pos <= right:
    x,y = s1.f(("time", pos))
    out.append((x,y,pos))
    #param = {'time':pos}
    #p0 = s1.find_interval_containing_value(param)
    #print "time:%s, xval:%s"%(pos,s1.get_x_for_param({'time':pos},p0, p0.next))
    pos += step_size
#  for p in s1.points:
#    print p, p.parameters
  for x,y,t in out:
    print x, y, t
  p0 = s1.points[0]
  p1 = s1.points[1]

  from IPython.Shell import IPShellEmbed
  ipshell = IPShellEmbed()
  ipshell()

if __name__ == "__main__":
  main()
