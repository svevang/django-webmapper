

def increment_lon(x, delta):
  "Return values in 180<x<-180"
  assert(x<=180.0 and x>=-180.0)
  x = x + 180.0
  x = x + delta
  x = x % 360.0
  return x - 180.0

def increment_lat(y, delta):
  assert(y<=90.0 and y>=-90)
  return y + delta
  # TODO bounds checking!
  
#  if y+delta > 
#  if delta > 360.0:
#    delta = delta % 360.0
#  if delta <= 90.0:
    
  
