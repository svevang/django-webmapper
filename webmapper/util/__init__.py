import os
import re

from django.contrib.gis.geos import GEOSGeometry
from django.contrib.gis.geos import MultiPolygon
from django.contrib.gis.geos import Polygon
from django.contrib.gis.geos import Point
from django.utils.functional import lazy
from django.utils.importlib import import_module
from django.core.urlresolvers import reverse
from django.template import Context, loader, TemplateDoesNotExist

split_caps = re.compile('(((?<=[a-z])[A-Z])|([A-Z](?![A-Z]|$)))')
camelcase_to_underscore = lambda str: split_caps.sub('_\\1', str).lower().strip('_')
lazy_reverse = lazy_reverse = lazy(reverse, unicode)

def get_class(path):
  try:
    module, classname = path.rsplit('.', 1)
  except ValueError:
    raise exceptions.ImproperlyConfigured('%s isn\'t a settings module' % path)
  try:
    mod = import_module(module)
  except ImportError, e:
    raise exceptions.ImproperlyConfigured('Error importing setting %s: "%s"' % (module, e))
  try:
    klass = getattr(mod, classname)
  except AttributeError:
    raise AttributeError('Module "%s" does not define a "%s" class' % (module, classname))
  return klass

def webmapper_eager_static_path(path):
  from django.conf import settings
  from django.contrib.sites.models import Site
  STATIC_URL = getattr(settings, "STATIC_URL")
  WEBMAPPER_MEDIA_URL = getattr(settings, "WEBMAPPER_MEDIA_URL", "http://{0}".format(Site.objects.get_current().domain))
  return os.path.join(WEBMAPPER_MEDIA_URL + STATIC_URL, 'webmapper', str(path))
webmapper_static_path = lazy(webmapper_eager_static_path, str)

def webmapper_path(*args, **kwargs):
  from django.conf import settings
  from django.contrib.sites.models import Site
  WEBMAPPER_MEDIA_URL = getattr(settings, "WEBMAPPER_MEDIA_URL", "http://{0}".format(Site.objects.get_current().domain))
  return os.path.join(WEBMAPPER_MEDIA_URL, reverse(*args, **kwargs)[1:])
webmapper_path = lazy(webmapper_path, str)

def get_setting(setting, override=None):
  """
  Get a setting from Django settings module, falling back to the
  default.

  If override is not None, it will be used instead of the setting.
  """
  from webmapper import defaults
  from django.conf import settings
  if override is not None:
    return override
  if hasattr(settings, 'WEBMAPPER_%s' % setting):
    resp = getattr(settings, 'WEBMAPPER_%s' % setting)
  else:
    resp = getattr(defaults, setting)
  if setting.startswith('API_'):
    # we're looking for a class 
    return get_class(resp)
  else:
    return resp


def bbox_from_naive(bbox):
  """
  Takes in a bbox that could span the international dateline and returns a 
  MultiPolygon that overlaps the same bounding area.
  """

  if bbox[0] > bbox[2]:
    # our bounding box crosses 180 degrees
    # so split into 2 adjoined bounding boxes
    west_poly = Polygon.from_bbox([bbox[0], bbox[1], 180, bbox[3]])
    east_poly = Polygon.from_bbox([-180, bbox[1], bbox[2], bbox[3]])
    # calculate the centroid
    balance_east = 180 - abs(east_poly.centroid.coords[0])
    balance_west = 180 - west_poly.centroid.coords[0]

    avg = balance_east - balance_west
    if balance_east > balance_west:
      cntr_lon = 180 - (balance_east - balance_west)
      # we are centered east
      cntr_lon = -1 * cntr_lon
    else:
      cntr_lon = 180 - (balance_west - balance_east)
    # both lats are the same
    centroid = Point(cntr_lon, east_poly.centroid.coords[1])

    # return MultiPolygon
    bbox_multi_poly = east_poly.union(west_poly)
  else:
    # EmailConfig.bbox expects a multipolygon
    bbox_multi_poly = MultiPolygon(Polygon.from_bbox(bbox))
    centroid = bbox_multi_poly.centroid

  return bbox_multi_poly, centroid


def find_backbone_templates(backbone_template_path, options={}):
  "returns a list of backbone templates rendered first as django templates"
  templates = []

  for root, dirs, files in os.walk(backbone_template_path):
    for f in files:
      if f.endswith('.tpl'):
        file_path = os.path.join(root, f)
        with open(file_path,'r') as fd:
          t = loader.get_template_from_string(fd.read())
          templates.append(t.render(Context({})))

  return templates



def register_backbone_templates(backbone_template_path, options={}):
  "returns a list of backbone templates rendered first as django templates"

  BACKBONE_TEMPLATES = get_setting('BACKBONE_TEMPLATES')

  for tpl in find_backbone_templates(backbone_template_path, options={}):
    if tpl not in BACKBONE_TEMPLATES:
      BACKBONE_TEMPLATES.append(tpl)

