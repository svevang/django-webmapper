from django.contrib.gis.geos import GEOSGeometry
from django.contrib.gis.geos import Point as GEOSPoint
from django import forms

from webmapper.models import Map
from webmapper.models import MapPane
from webmapper.models import GoogleMapOptionSet
from webmapper.models import Point

class MapForm(forms.ModelForm):

  title = forms.CharField(required=False)
  description = forms.CharField(required=False)

  class Meta:
    model = Map
    fields = ('title', 'description', 'user')

class MapPaneForm(forms.ModelForm):

  def clean(self, *args, **kwargs):
    return super(MapPaneForm,self).clean(*args, **kwargs)

  class Meta:
    model = MapPane
    exclude = ('map', 'kml_layers','layers')

class GoogleMapOptionSetForm(forms.ModelForm):

  def clean(self, *args, **kwargs):
    return super(GoogleMapOptionSetForm, self).clean(*args, **kwargs)

  class Meta:
    model = GoogleMapOptionSet
    exclude = ['map_pane']

class PointForm(forms.ModelForm):

  class Meta:
    model = Point

