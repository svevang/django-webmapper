import logging

from webmapper.util import camelcase_to_underscore
from django.db.models import Q

custom_handler_registry = {
}


class CustomHandler(object):

  def get(self, request, api_output, resources):
    raise NotImplementedError

  def post(self, request, api_output, resources):
    raise NotImplementedError

  def put(self, request, api_output, resources):
    raise NotImplementedError

  def delete(self, request, api_output, resources):
    raise NotImplementedError

  def list(self, request, api_output, resources):
    # return a Q object that filters this request
    return Q()

def register(handler_name, http_verb, handler_func):

  http_verb = http_verb.upper()

  base_verbs = ('GET', 'POST', 'PUT', 'DELETE')
  assert http_verb in base_verbs

  if handler_name in custom_handler_registry:
    custom_handler_registry[handler_name][http_verb].append(handler_func)
  else:
    custom_handler_registry[handler_name] = {}
    for base_verb in base_verbs:
      custom_handler_registry[handler_name][base_verb] = []

    custom_handler_registry[handler_name][http_verb].append(handler_func)
