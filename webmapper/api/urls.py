from django.conf.urls.defaults import *
from django.conf import settings

from piston.resource import Resource

from webmapper.api.handlers import MapHandler
from webmapper.api.handlers import MapPaneHandler
from webmapper.api.handlers import KMLLayerHandler
from webmapper.api.handlers import PointHandler
from webmapper.api.handlers import PolygonHandler
from webmapper.api.handlers import PolyLineHandler

from webmapper.util import get_setting

AuthStrategy = get_setting('API_AUTH')

class WebmapperResource(Resource):

  def __init__(self,*args, **kwargs):
    res = super(WebmapperResource, self).__init__(*args, **kwargs)
    # changing piston's default behavior
    self.email_errors = getattr(settings, 'PISTON_EMAIL_ERRORS', not settings.DEBUG)
    self.display_errors = getattr(settings, 'PISTON_DISPLAY_ERRORS', settings.DEBUG)
    return res

map_resource = WebmapperResource(handler=MapHandler, authentication=AuthStrategy())
mappane_resource = WebmapperResource(handler=MapPaneHandler, authentication=AuthStrategy())
kml_layer_resource = WebmapperResource(handler=KMLLayerHandler, authentication=AuthStrategy())
point_resource = WebmapperResource(handler=PointHandler, authentication=AuthStrategy())
polygon_resource = WebmapperResource(handler=PolygonHandler, authentication=AuthStrategy())
polyline_resource = WebmapperResource(handler=PolyLineHandler, authentication=AuthStrategy())


urlpatterns = patterns('',

  url(r'^maps/$', map_resource, name="api_maps"),
  url(r'^maps/(?P<map_id>\d+|new)/$', map_resource, name="api_map"),

  url(r'^map_panes/$', mappane_resource, name="api_map_panes"),
  url(r'^map_panes/(?P<map_pane_id>\d+|new)/$', mappane_resource, name="api_map_pane"),

  url(r'^kml_layers/$', kml_layer_resource, name="api_kml_layers"),
  url(r'^kml_layers/(?P<kml_layer_id>\d+)/$', kml_layer_resource, name="api_kml_layer"),

  url(r'^points/$', point_resource, name="api_points"),
  url(r'^points/(?P<point_id>\d+)/$', point_resource, name="api_point"),

  url(r'^polygons/$', polygon_resource, name="api_polygons"),
  url(r'^polygons/(?P<polygon_id>\d+)/$', polygon_resource, name="api_polygon"),

  url(r'^polylines/$', polyline_resource, name="api_polylines"),
  url(r'^polylines/(?P<polyline_id>\d+)/$', polyline_resource, name="api_polyline"),

)

