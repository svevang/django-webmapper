from django.conf import settings
from django.conf import settings
from django.core import exceptions, urlresolvers
from django.http import HttpResponseRedirect
from django.db.models import Q

from piston.utils import rc

from webmapper.util import get_setting

# django 1.4
try:
  from django.utils.html import smart_urlquote as urlquote
except ImportError:
  from django.utils.html import urlquote

class BaseAuth(object):

  def __init__(self, login_url=settings.LOGIN_URL, redirect_field_name='next'):
    self.login_url = login_url
    self.redirect_field_name = redirect_field_name
    self.request = None

  def is_authenticated(self, request):
    raise NotImplementedError

  def challenge(self):
    """
    `challenge`: In cases where `is_authenticated` returns
    False, the result of this method will be returned.
    This will usually be a `HttpResponse` object with
    some kind of challenge headers and 401 code on it.
    """
    path = urlquote(self.request.get_full_path())
    tup = self.login_url, self.redirect_field_name, path 
    return HttpResponseRedirect('%s?%s=%s' %tup)

class UserAuth(BaseAuth):

  def is_authenticated(self, request):
    self.request = request
    return request.user.is_authenticated()

class PublicAuth(BaseAuth):

  def is_authenticated(self, request):
    return True 

class ApiPermissions(object):

  def __init__(self, *args, **kwargs):

    super(ApiPermissions, self).__init__(*args, **kwargs)
    # essentially a cache of the urlconf
    # once the regex's are compiled, should be fast
    urlconf = settings.ROOT_URLCONF
    urlresolvers.set_urlconf(urlconf)
    self.resolver = urlresolvers.RegexURLResolver(r'^/', urlconf)

  def can_list(self, request):
    WEBMAPPER_API_AUTH = get_setting('API_AUTH')
    auth = WEBMAPPER_API_AUTH()
    return auth.is_authenticated(request)

  def list_resource(self, request):
    return self.klass.objects.all().filter(self.list_resource_kwargs(request))

  def can_read(self, request, is_listing=False, is_new=False):

    if is_new:
      # we are just returning a representation of what the model looks like
      return True
    elif is_listing:
      # The user can potentially list anything, at least an empty list.
      # Method `list_resource` restricts what a user can fetch in list form.
      return self.can_list(request)
    else:
      # Permissions
      wmap = self.resolve_map(request)
      # The conditions here are isomorphic to the restrictions upon `list_resource`
      if (wmap and wmap.user == request.user) or \
         (wmap and (wmap.user is None)) or \
         request.user.is_superuser:
        return True

    return False

  def can_delete(self, request):

    wmap = self.resolve_map(request)
    if wmap and wmap.user == request.user:
      return True
    return False

  def can_create(self, request):
    # for now assume all authenticated users on the site can create map content
    if request.user.is_authenticated():
      return True
    return False
    
  def can_update(self, request):
    wmap = self.resolve_map(request)
    if wmap and wmap.user == request.user:
      return True
    return False

  def resource_from_request(self, klass, kwarg_key, request):

    # The idea here is that we need a consistent way to fetch the parameters
    # used in querying for our webmapper resources. Use the patterns in the api
    # urlconf to litely sanitize the parameters and fetch the resource.

    # This is a reverse lookup on the resource URL to fetch the resource.
    # Resolver is the cached url conf constructed in __init__.
    # Look up the request url and pull out the api specific kwargs.
    kwargs = self.resolver.resolve(request.META['PATH_INFO']).kwargs
    # if the kwarg_key is equal to "new", 
    # if our model's kwarg_key is present 
    if kwarg_key in kwargs and not kwargs[kwarg_key] == "new":
      obj = klass.objects.get(id=kwargs[kwarg_key])
      return obj
    return None
         
class MapPermissions(ApiPermissions):

  def __init__(self, *args, **kwargs):
    from webmapper.models import Map
    self.klass = Map
    self.url_pattern_kwarg = 'map_id'
    return super(MapPermissions, self).__init__(*args, **kwargs)

  def resolve_map(self, request):
    'Resolve the Map object from the current webmapper resource'

    wmap = self.resource_from_request(self.klass, self.url_pattern_kwarg, request)
    return wmap

  def list_resource_kwargs(self, request):
    return Q(user=request.user.id)|Q(user__isnull=True)

# The Permissions classes below all lookup the user in the same relative fashion
# e.g. map__user

class BaseGeoPermissions(ApiPermissions):

  def list_resource_kwargs(self, request):

    return Q(map__user=request.user.id)|Q(map__user__isnull=True)

class MapPanePermissions(BaseGeoPermissions):

  def __init__(self, *args, **kwargs):
    from webmapper.models import MapPane
    self.klass = MapPane
    self.map_pane_class = MapPane
    self.url_pattern_kwarg = 'map_pane_id'
    return super(MapPanePermissions, self).__init__(*args, **kwargs)

  def resolve_map(self, request):
    'Resolve the Map object from the current webmapper resource'

    map_pane = self.resource_from_request(self.klass, self.url_pattern_kwarg, request)
    if map_pane is None:
      return None 
    return map_pane.map

class KMLLayerPermissions(BaseGeoPermissions):

  def __init__(self, *args, **kwargs):
    from webmapper.models import KMLLayer
    self.klass = KMLLayer 
    self.url_pattern_kwarg = 'kml_layer_id'
    return super(KMLLayerPermissions, self).__init__(*args, **kwargs)

  def resolve_map(self, request):
    'Resolve the Map object from the current webmapper resource'

    kml_layer = self.resource_from_request(self.klass, self.url_pattern_kwarg, request)
    if kml_layer is None:
      return None 
    return kml_layer.map

class PointPermissions(BaseGeoPermissions):

  def __init__(self, *args, **kwargs):
    from webmapper.models import Point
    self.klass = Point 
    self.url_pattern_kwarg = 'point_id'
    return super(PointPermissions, self).__init__(*args, **kwargs)

  def resolve_map(self, request):
    'Resolve the Map object from the current webmapper resource'

    point = self.resource_from_request(self.klass, self.url_pattern_kwarg, request)
    if point is None:
      return None 
    return point.map

class PolygonPermissions(BaseGeoPermissions):

  def __init__(self, *args, **kwargs):
    from webmapper.models import Polygon
    self.klass = Polygon
    self.url_pattern_kwarg = 'polygon_id'
    return super(PolygonPermissions, self).__init__(*args, **kwargs)

  def resolve_map(self, request):
    'Resolve the Map object from the current webmapper resource'

    polygon = self.resource_from_request(self.klass, self.url_pattern_kwarg, request)

    if polygon is None:
      return None 
    return polygon.map

class PolyLinePermissions(BaseGeoPermissions):

  def __init__(self, *args, **kwargs):
    from webmapper.models import PolyLine
    self.klass = PolyLine 
    self.url_pattern_kwarg = 'polyline_id'
    return super(PolyLinePermissions, self).__init__(*args, **kwargs)

  def resolve_map(self, request):
    'Resolve the Map object from the current webmapper resource'

    polyline = self.resource_from_request(self.klass, self.url_pattern_kwarg, request)

    if polyline is None:
      return None 
    return polyline.map
