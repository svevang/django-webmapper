import os
import simplejson

from django.conf import settings
from django.contrib.sites.models import Site
from django.core.urlresolvers import reverse
from django.contrib.gis.geos import Point as GeosPoint
from django.contrib.gis.geos import GEOSGeometry
from django.template import RequestContext, Context, loader
from django.forms.models import model_to_dict

from piston.handler import BaseHandler
from piston.utils import rc
from piston.utils import validate


from webmapper.api.forms import MapForm
from webmapper.api.forms import MapPaneForm
from webmapper.api.forms import GoogleMapOptionSetForm
from webmapper.api.forms import PointForm

from webmapper.api.custom_handlers import custom_handler_registry

from webmapper.forms.webmap import KMLLayerForm

from webmapper.models import Map
from webmapper.models import MapPane
from webmapper.models import KMLLayer
from webmapper.models import Point
from webmapper.models import PolyLine
from webmapper.models import Polygon
from webmapper.models import GoogleMapOptionSet
from webmapper.models import OpenLayersOptionSet

from webmapper.util import get_setting
from webmapper.util import camelcase_to_underscore

MapPermissionStrategy      = get_setting('API_MAP_PERM')
MapPanePermissionStrategy  = get_setting('API_MAP_PANE_PERM')
KMLLayerPermissionStrategy = get_setting('API_KML_LAYER_PERM')
PointPermissionStrategy    = get_setting('API_POINT_PERM')
PolygonPermissionStrategy  = get_setting('API_POLYGON_PERM')
PolyLinePermissionStrategy  = get_setting('API_POLYLINE_PERM')


class CustomizableHandler(BaseHandler):

  def list_resource(self, request):

    resource_qs = self.permission_strategy.list_resource(request)
    return resource_qs
  
  @classmethod
  def model_to_dict(cls, instance):

    model_dict = model_to_dict(instance)
    if model_dict['id'] is None:
      del model_dict['id']
    return model_dict

  def api_response(self, request, response, api_output):
    """
    Wrap the api response, allowing custom code to hook in.

    api_output: A dictionary mapping between the django models directly 
                    related to this api call and the api specific 
                    representation that is produced.
    """
    from webmapper.api.custom_handlers import custom_handler_registry
    for handler in request.GET.getlist('handler'):
        # handler: An instance of a webmapper.api.custom_handlers.CustomHandler
        if request.method.upper() in custom_handler_registry[handler]:
          for callback in custom_handler_registry[handler][request.method.upper()]:
            response = callback(request, response, api_output)

    return response

  def BAD_REQUEST_form_errors(self, invalid_forms):
    errors = {}
    for form in invalid_forms:
      errors.update(form.errors)
    resp = rc.BAD_REQUEST
    resp.content = ''
    resp.write(simplejson.dumps({'errors':errors}))
    print "inside form errors:", resp.content
    return resp

class MapHandler(CustomizableHandler):

  model = Map
  exclude = []
  allowed_methods = ('GET', 'POST', 'PUT', 'DELETE')
  permission_strategy = MapPermissionStrategy()

  @classmethod
  def build_model_output(cls, wmap):
    wmap_dict = cls.model_to_dict(wmap)
    wmap_dict['map_pane_id'] = wmap_dict['map_pane']
    del wmap_dict['map_pane']
    return wmap_dict 

  def create(self, request, map_id=None):

    if not self.permission_strategy.can_create(request):
      return rc.FORBIDDEN

    # because we are dealing with the semantics of unsaved
    # maps, we have to handle map_id kwargs, but only if it is "new"
    if not (map_id is None or map_id == "new"):
      return rc.BAD_REQUEST

    if request.content_type:
      map_data = request.data
      map_data['user'] = request.user.id
      map_form = MapForm(map_data)
      if map_form.is_valid():
        wmap = map_form.save()
      else:
        return self.BAD_REQUEST_form_errors([map_form])
    else:
      wmap = Map(user=request.user)
      wmap.save()

    response =  self.build_model_output(wmap)

    return self.api_response(request, response, api_output={wmap: response})

  def read(self, request, map_id=None):

    is_new = map_id == "new"
    if not self.permission_strategy.can_read(request, 
                                             is_listing=map_id is None, 
                                             is_new=is_new):
        return rc.FORBIDDEN

    if is_new:
      return self.build_model_output(Map())

    api_output = {}
    if map_id:
      wmap = Map.objects.get(pk=map_id)
      wmap_output = self.build_model_output(wmap)
      api_output[wmap] = wmap_output
      response = wmap_output
    else:
      # list resource
      for wmap in self.list_resource(request):
        api_output[wmap] = self.build_model_output(wmap)
        
      response = api_output.values()

    return self.api_response(request, response, api_output)

  def update(self, request, map_id=None):

    if not self.permission_strategy.can_update(request):
      return rc.FORBIDDEN

    # per the piston example, we check if the content_type is set
    if not (request.content_type and map_id):
      resp = rc.BAD_REQUEST
      resp.write('Requires a map_id and content_type')
      return resp

    map_data = request.data

    # fetch the object in question
    wmap = Map.objects.get(pk=map_id)

    # build the forms
    map_form = MapForm(map_data, instance=wmap)

    if map_form.is_valid():
      map_form.save()
      api_output = {}
      response = self.build_model_output(wmap)
      api_output[wmap] = response
      return self.api_response(request, response, api_output)
    else:
      return self.BAD_REQUEST_form_errors([map_form])

  def delete(self, request, map_id=None):

    if map_id is None:
      return rc.FORBIDDEN

    if self.permission_strategy.can_delete(request):
        wmap = self.list_resource(request).get(id=map_id)
        response = {'ok':200}
        processed_response = self.api_response(request, response, api_output={wmap:None})
        wmap.delete()
        return processed_response
    else:
      return rc.FORBIDDEN

class MapPaneHandler(CustomizableHandler):

  #model = MapPane
  allowed_methods = ('GET', 'PUT', 'DELETE')
  exclude = []
  permission_strategy = MapPanePermissionStrategy()

  @classmethod
  def build_model_output(cls, map_pane):
    """
       Returns a dictionary representing the map_pane "resource"
       Must be able to represent unsaved models
    """

    map_pane_dict = cls.model_to_dict(map_pane)
    pane_id = map_pane_dict.get('id', None)

    # Google Map Options
    try:
      goog_options = cls.model_to_dict(map_pane.google_options)
    except GoogleMapOptionSet.DoesNotExist:
      goog_options = cls.model_to_dict(GoogleMapOptionSet())

    map_pane_dict['google_options'] =  goog_options
    map_pane_dict['google_options']['zoom'] = int(map_pane_dict['zoom'])

    # Openlayers Options
    try:
      openlayers_options = cls.model_to_dict(map_pane.openlayers_map_options)
    except OpenLayersOptionSet.DoesNotExist:
      openlayers_options = cls.model_to_dict(OpenLayersOptionSet())

    map_pane_dict['openlayers_options'] =  openlayers_options
    map_pane_dict['openlayers_options']['zoom'] = int(map_pane_dict['zoom'])

    pnt = map_pane_dict['center']
    map_pane_dict['center'] = simplejson.loads(pnt.geojson)
    map_pane_dict['center_srid'] = pnt.srid

    # related layer kml
    # TODO, remap the kml layers from the map to the map_pane
    # which makes it easier to query
    all_kml_ids = KMLLayer.objects.values('id').filter(map__map_pane=pane_id)
    active_kml_ids = all_kml_ids.filter(map_panes_kml_layers__id=pane_id) 
    map_pane_dict['all_kml_layer_ids'] = [x['id'] for x in all_kml_ids]
    map_pane_dict['active_kml_layer_ids'] = [x['id'] for x in active_kml_ids]

    return map_pane_dict

  def read(self, request, map_pane_id=None):

    is_new = map_pane_id == "new"
    if not self.permission_strategy.can_read(request, 
                                             is_listing=map_pane_id is None,
                                             is_new=is_new):
        return rc.FORBIDDEN

    if is_new:
      return self.build_model_output(MapPane())

    map_pane_list = self.list_resource(request)

    api_output = {}
    if map_pane_id:
      map_pane = map_pane_list.get(pk=map_pane_id)
      response = self.build_model_output(map_pane)
      api_output[map_pane] = response
    else:
      if 'map_id' in request.GET:
        # list panes associated with a particular map
        for map_pane in map_pane_list.filter(map=request.GET['map_id']):
          api_output[map_pane] = self.build_model_output(map_pane) 
      else:
        for map_pane in map_pane_list.all():
          api_output[map_pane] = self.build_model_output(map_pane) 
      response = api_output.values()

    return self.api_response(request, response, api_output)

  def update(self, request, map_pane_id=None):

    if not self.permission_strategy.can_update(request):
      return rc.FORBIDDEN

    # per the piston example, we check if the content_type is set
    if not (request.content_type and map_pane_id):
      resp = rc.BAD_REQUEST
      resp.write('Requires a map_pane_id and content_type')
      return resp

    map_pane_data = request.data
    map_pane_data['center'] = GeosPoint(map_pane_data['center']['coordinates'])

    # fetch the object in question
    map_pane = MapPane.objects.get(pk=map_pane_id)

    # build the forms
    map_pane_form = MapPaneForm(map_pane_data, instance=map_pane)
    google_options_form = GoogleMapOptionSetForm(map_pane_data['google_options'],
                                                  instance=map_pane.google_options)

    if map_pane_form.is_valid() and google_options_form.is_valid():
      map_pane_form.save()
      google_options_form.save()
      response = self.build_model_output(map_pane)
      return self.api_response(request, response, api_output={map_pane: response})
    else:
      return self.BAD_REQUEST_form_errors([map_pane_form, google_options_form])


  def create(self, request):

    # for now the creation of a map pane is handled my the map api endpoint
    # POST is not set for this handler

    if not self.permission_strategy.can_create(request):
      return rc.FORBIDDEN

    map = Map()
    map.save()
    map_pane = MapPane(map=map)
    map_pane.save()
    response = self.build_model_output(map_pane)
    return self.api_response(request, response, api_output={map_pane:response})


class KMLLayerHandler(CustomizableHandler):

  model = KMLLayer
  exclude = ()
  permission_strategy = KMLLayerPermissionStrategy()

  @classmethod
  def build_model_output(cls, kml_layer):

    kml_layer_dict = cls.model_to_dict(kml_layer)
    current_domain = Site.objects.get(id=settings.SITE_ID).domain

    kml_layer_dict['map_pane_ids'] = [mp['id'] 
        for mp in MapPane.objects.values('id').filter(kml_layers__in=[kml_layer_dict['id']])]

    if kml_layer_dict['kml']:
        path = os.path.join(settings.MEDIA_URL, kml_layer_dict['kml'].url )
        parameters = "?r=%s"%kml_layer_dict['revision']
        kml_layer_dict['kml_url'] = "http://" + current_domain + path + parameters
    else:
        kml_layer_dict['kml_url'] = None

    return kml_layer_dict

  def read(self, request, kml_layer_id=None):

    if not self.permission_strategy.can_read(request, is_listing=kml_layer_id is None):
        return rc.FORBIDDEN

    kml_layer_list = self.list_resource(request) 

    api_output = {} 
    if kml_layer_id:
      kml_layer = KMLLayer.objects.get(pk=kml_layer_id)
      response = self.build_model_output(kml_layer)
      api_output[kml_layer] = response
    else:
      if 'map_id' in request.GET:
        for kml_layer in kml_layer_list.filter(map=request.GET['map_id']):
          api_output[kml_layer] = self.build_model_output(kml_layer)
      else:
        for kml_layer in kml_layer_list.all():
          api_output[kml_layer] = self.build_model_output(kml_layer)
      response = api_output.values()

    return self.api_response(request, response, api_output)

  def create(self, request):

    if not self.permission_strategy.can_create(request):
      return rc.FORBIDDEN

    map_id = request.GET.get('map_id', None)
    try:
      wmap = Map.objects.get(id=map_id)
    except Map.DoesNotExist:
      resp = rc.BAD_REQUEST
      resp.write('map {0} does not exist'.format(map_id))
      return resp

    validated_forms = []
    invalid_forms = []
    for temp_file in request.FILES.values():
      # examine KML file 
      form = KMLLayerForm({
          'map':wmap.pk,
          'title':temp_file.name,
          'revision':0,
        }, {'kml':temp_file})
      if form.is_valid():
        validated_forms.append(form)
      else:
        invalid_forms.append(form)

    if invalid_forms:
      return self.BAD_REQUEST_form_errors(invalid_forms)

    api_output = {}
    for form in validated_forms:
      kml_layer = form.save()
      api_output[kml_layer] = self.build_model_output(kml_layer)
    response = api_output.values()
    return self.api_response(request, response, api_output)


class PointHandler(CustomizableHandler):

  model = Point
  exclude = ()
  allowed_methods = ('GET', 'POST', 'PUT', 'DELETE')
  permission_strategy = PointPermissionStrategy()

  @classmethod
  def build_model_output(cls, point):
    point_dict = {}
    point_dict['id'] = point.id
    point_dict['geojson'] = point.geojson
    return point_dict

  def read(self, request, point_id=None):

    if not self.permission_strategy.can_read(request, is_listing=point_id is None):
        return rc.FORBIDDEN

    point_list = self.list_resource(request).geojson()

    api_output = {}
    if point_id:
      point = point_list.get(pk=point_id)
      response = self.build_model_output(instance)
      api_output[point] = response
    else:
      if 'map_id' in request.GET:
        for point in point_list.filter(map=request.GET['map_id']):
          api_output[point] = self.build_model_output(point)
      else:
        for point in point_list.all():
          api_output[point] = self.build_model_output(point)

      response = api_output.values()

    return self.api_response(request, response, api_output)

  def create(self, request):

    if not self.permission_strategy.can_create(request):
      return rc.FORBIDDEN

    if 'map_id' in request.GET:
      wmap = Map.objects.get(id=int(request.GET['map_id']))
      if not wmap.user == request.user:
        return rc.BAD_REQUEST
    else:
      return rc.BAD_REQUEST

    point_dict = request.data
    point_dict['point'] = GeosPoint(point_dict['geojson']['coordinates'])
    point_dict['map'] = wmap.id 
    point_form = PointForm(point_dict)

    if point_form.is_valid():
      point = point_form.save()
      point.geojson = point_dict['geojson']
    else:
      return self.BAD_REQUEST_form_errors([point_form])

    response =  self.build_model_output(point)

    return self.api_response(request, response, api_output={point: response})

  def update(self, request, point_id=None):

    if not self.permission_strategy.can_update(request):
      return rc.FORBIDDEN

    if not (request.content_type and point_id):
      resp = rc.BAD_REQUEST
      resp.write('Requires a point_id and content_type')
      return resp

    point_dict = request.data

    if "multipart/form-data" in  request.content_type:
      # hack, complex objects need to be retrieved when encoded
      point_dict['geojson'] = simplejson.loads(point_dict['geojson'])

    # fetch the object in question
    point = Point.objects.select_related().get(pk=point_id)

    # now build up our dict
    point_dict['point'] = GeosPoint(point_dict['geojson']['coordinates'])
    point_dict['map'] = point.map_id


    # build the forms
    point_form = PointForm(point_dict, instance=point)

    if point_form.is_valid():
      point_form.save()
      api_output = {}
      point.geojson = point.point.geojson
      response = self.build_model_output(point)
      api_output[point] = response
      return self.api_response(request, response, api_output)
    else:
      return self.BAD_REQUEST_form_errors([point_form])

class PolygonHandler(CustomizableHandler):

  model = Polygon
  exclude = ()
  permission_strategy = PolygonPermissionStrategy()

  @classmethod
  def build_model_output(cls, polygon):
    polygon_dict = {}
    polygon_dict['id'] = polygon.id
    polygon_dict['geojson'] = polygon.geojson
    return polygon_dict

  def read(self, request, polygon_id=None):

    if not self.permission_strategy.can_read(request, is_listing=polygon_id is None):
        return rc.FORBIDDEN

    polygon_list = self.list_resource(request).geojson()

    api_output = {}
    if polygon_id:
      polygon = polygon_list.get(pk=polygon_id)
      response = self.build_model_output(polygon)
      api_output[polygon] = response
    else:
      if 'map_id' in request.GET:
        for polygon in polygon_list.filter(map=request.GET['map_id']):
          api_output[polygon] = self.build_model_output(polygon)
      else:
        for polygon in polygon_list.all():
          api_output[polygon] = self.build_model_output(polygon)
      response = api_output.values()
    return self.api_response(request, response, api_output)


class PolyLineHandler(CustomizableHandler):

  model = PolyLine
  exclude = ()
  permission_strategy = PolyLinePermissionStrategy()

  @classmethod
  def build_model_output(cls, polyline):
    polyline_dict = {}
    polyline_dict['id'] = polyline.id
    polyline_dict['geojson'] = polyline.geojson
    return polyline_dict

  def read(self, request, polyline_id=None):

    if not self.permission_strategy.can_read(request, is_listing=polyline_id is None):
        return rc.FORBIDDEN

    polyline_list = self.list_resource(request).geojson()

    api_output = {}
    if polyline_id:
      polyline = polyline_list.get(pk=polyline_id)
      response = self.build_model_output(polyline)
      api_output[polyline] = response
    else:
      if 'map_id' in request.GET:
        for polyline in polyline_list.filter(map=request.GET['map_id']):
          api_output[polyline] = self.build_model_output(polyline)
      else:
        for polyline in polyline_list.all():
          api_output[polyline] = self.build_model_output(polyline)
      response = api_output.values()
    return self.api_response(request, response, api_output)

