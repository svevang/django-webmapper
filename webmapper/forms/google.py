from django import forms
from webmapper.models import GoogleMapOptionSet

class GoogleMapOptionSetAdminForm(forms.ModelForm):

  map_type = forms.ChoiceField(choices=GoogleMapOptionSet.MAP_TYPES, empty="")

  def __init__(self, *args, **kwargs):
    res = super(GoogleMapOptionSetAdminForm, self).__init__(*args, **kwargs)
    self.fields['map_type'].initial = ("TERRAIN","TERRAIN")
    return res

  class Meta:
    model = GoogleMapOptionSet

