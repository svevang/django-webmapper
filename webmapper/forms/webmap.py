import os

from django import forms
from webmapper.models import Map
from webmapper.models import KMLLayer

from xml.dom.minidom import parse, parseString
from lxml import etree
from lxml.etree import XMLSyntaxError

class KMLLayerForm(forms.ModelForm):

  def clean(self):
    cleaned_data = self.cleaned_data
    file_ref = cleaned_data.get("kml")
    url_ref = cleaned_data.get("kml_url")

    if 'kml' in self._errors or 'url_ref' in self._errors:
      return cleaned_data

    if (file_ref and url_ref) or (not file_ref and not url_ref):
      msg = "Please select just a file or just a URL"
      self._errors["kml"] = self.error_class([msg])
      self._errors["kml_url"] = self.error_class([msg])
      del cleaned_data['kml']
      del cleaned_data['kml_url']
    return cleaned_data

  def clean_kml(self):
    kml_file = self.cleaned_data.get("kml")
    kml_file.seek(0)

    try:
      tree = etree.parse(kml_file)
    except XMLSyntaxError, e:
      raise forms.ValidationError(str(e))

    kml_file.seek(0)
    return kml_file 

  class Meta:
    model = KMLLayer

class MapForm(forms.ModelForm):
  
  class Meta:
    fields = ('title', 'description')
    model = Map
