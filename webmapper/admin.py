from django.contrib import admin
from django.contrib.gis.admin import GeoModelAdmin, OSMGeoAdmin
from webmapper.models import (Point, PolyLine, Polygon, Map, MapPane, KMLLayer, GoogleMapOptionSet, GPX)
from webmapper.forms.webmap import KMLLayerForm

class PointAdmin(GeoModelAdmin):
  pass

class PolyLineAdmin(GeoModelAdmin):
  pass

class PolygonAdmin(GeoModelAdmin):
  pass

class KMLLayerAdmin(GeoModelAdmin):
  form = KMLLayerForm

class LayerAdmin(GeoModelAdmin):
  pass

###########################################

class KMLLayerInlineAdmin(admin.StackedInline):
  model = KMLLayer
  extra = 0
  form = KMLLayerForm

class GoogleMapOptionSetAdmin(admin.StackedInline):
  model = GoogleMapOptionSet
  max_num = 1
  extra = 0

#######################################
class MapAdmin(GeoModelAdmin):
  inlines = [KMLLayerInlineAdmin]
  display_wkt = True

  def add_view(self, request, form_url='', extra_context=None):
    from webmapper.views import map_create

    return map_create(request, extra_context, template="")



class LayerAdmin(GeoModelAdmin):
  display_wkt = True

class KMLLayerAdmin(GeoModelAdmin):
  display_wkt = True


#admin.site.register(Polygon, PolygonAdmin)
#admin.site.register(PolyLine, PolyLineAdmin)
#admin.site.register(Point, PointAdmin)
#admin.site.register(GPX)
#admin.site.register(Map, MapAdmin)
#admin.site.register(KMLLayer, KMLLayerAdmin)
#admin.site.register(Layer, LayerAdmin)
