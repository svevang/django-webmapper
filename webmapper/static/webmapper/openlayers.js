window.webmapper = (function(webmapper){

  webmapper.openlayers = {}

  webmapper.openlayers.MapPaneView = webmapper.AbstractMapPaneView.extend({

    initialize:function(){
      webmapper.AbstractMapPaneView.prototype.initialize.call(this)
      this.epsg900913Proj = new OpenLayers.Projection("EPSG:900913")
      this.epsg4326Proj = new OpenLayers.Projection("EPSG:4326")
    },

    renderMapWidget: function(){

      // override model scrolling config
      /*i.f(this.model.map.options.scrollZoom){
       TODO scroll wheen OL options
        scrollwheel = false
      }*/
      var olOpts = this.model.get('openlayers_options')
      this.openLayersMap = new OpenLayers.Map({
        div: this.el,
        allOverlays: true,
        projection: this.epsg900913Proj
        //units: "m"
      })

      var olMapPaneView = this
      var osmLayer = new OpenLayers.Layer.OSM()

      osmLayer.events.on({'loadend': function(){ 
        olMapPaneView.trigger('tiles-loaded') }
      })

      this.openLayersMap.addLayers([ osmLayer ])
      var coords = this.model.get('center').coordinates
      // assume model is always in EPSG 4326
      var ctr = new OpenLayers.LonLat(coords[0], coords[1])
      ctr.transform(this.epsg4326Proj, this.openLayersMap.getProjectionObject())

      this.openLayersMap.size = new OpenLayers.Size(0,0)
      // setCenter takes in epsg 900913 and converts it to epsg 4326
      this.openLayersMap.setCenter(ctr, this.model.get('zoom'), {})
      this.openLayersMap.updateSize()


    },

    removeMapWidget: function(){

      if(this.openLayersMap){
        this.openLayersMap.destroy()
      }
      $(this.el).empty()

    },

    bindMapWidgetEvents: function(){

      _.bindAll(this, "propagateCenter", "propagateZoom", "propagateLayerId")
        this.openLayersMap.events.register("moveend",     this.openLayersMap,  this.propagateCenter)
        this.openLayersMap.events.register("zoomend",     this.openLayersMap,  this.propagateZoom)
        this.openLayersMap.events.register("changelayer", this.openLayersMap,  this.propagateLayerId)

        // webmapper event provider
        this.openLayersMap.events.register("click",
                                           this.openLayersMap,
                                           this._eventAttrConstructor('click', this.processMapClick))
        _.bindAll(this)
        this.on('reflow', this.reflow)
    },

    syncMapWidget:function(){
    },
    // synchronize the MapPane server state to the map widget 

    attachKML:function(kmlCollection){
      var mapPaneView = this

      kmlCollection.each(function(kmlLayer){
        kmlLayer.view = new webmapper.openlayers.KmlView({model: kmlLayer, 
                                                          mapPaneView:mapPaneView }) 
        kmlLayer.view.render()
      })
    },

    attachPoints:function(pointCollection){
      var mapPaneView = this

      if(typeof mapPaneView.markers == "undefined"){
        mapPaneView.markers = new OpenLayers.Layer.Markers("Markers")
        mapPaneView.markers.projection = mapPaneView.openLayersMap.getProjectionObject()
        mapPaneView.openLayersMap.addLayer(mapPaneView.markers)
      }

      pointCollection.each(function(point){
        point.mapPane = mapPaneView.model
        point.markerView = new webmapper.openlayers.MarkerView({model:point,
                                                                mapPaneView:mapPaneView})
        point.markerView.render()
      })

    },

    attachPolygons:function(polygonCollection){

      var mapPaneView = this
      polygonCollection.each(function(polygon){

        polygon.polygonView = new webmapper.openlayers.PolygonView({model:polygon, 
                                                                    mapPaneView:mapPaneView})
        polygon.polygonView.render()

      })

    },

    getBounds:function(){
    /*var bounds = this.googleMapsMap.getBounds()
      var sw = bounds.getSouthWest()
      var ne = bounds.getNorthEast()
      console.log(sw, ne)
      return [[sw.lng(), sw.lat()],[ne.lng(), ne.lat()]]*/

      var b =  this.openLayersMap.getExtent()
      b.transform(this.openLayersMap.getProjectionObject(), this.epsg4326Proj)
      return  b.toArray()
      // nest the corner points in their own array
    },
    getLayerId:function(){
      return "noop"
    },

    propagateCenter:function(){

      var cntr = this.openLayersMap.getCenter()
      cntr.transform(this.epsg900913Proj, this.epsg4326Proj)

      // create a copy so that we can fire the change event (ptr compare)
      var newCenter = _.extend({}, this.model.get('center'))
      newCenter.coordinates = [cntr.lon, cntr.lat]
      this.model.set({center:newCenter})

    },

    propagateZoom:function(){
      console.log('openlayers event: zoom', this.openLayersMap.getZoom())
      this.model.set({zoom: this.openLayersMap.getZoom()})
    },

    propagateLayerId:function(){
      // noop until we have a openlayers options container
    },

    reflow:function(){
      // redraw the map upon container resize
      this.openLayersMap.updateSize()
    },

    paneDiv: function(){
      // return the principle map widget container el
      return this.openLayersMap.div
    },

    /* Event handlers */
    // process proprietary events -> webmapper events
    processMapClick: function(click){
      var latLon = this.openLayersMap.getLonLatFromViewPortPx(click.xy)
      latLon.transform(this.openLayersMap.getProjectionObject(), this.epsg4326Proj)
      return new webmapper.events.MapClick({lat:latLon.lat,
                                            lon:latLon.lon,
                                            mapPane: this.model,
                                            proprietary:click})
    }
  })

  webmapper.openlayers.MarkerView = webmapper.AbstractMarkerView.extend({

    olMarker:null,

    render:function(){

        var olMarkerView = this

        var coords = this.model.get('geojson').coordinates

        // save a reference to the point for the info bubble
        this.olPoint = new OpenLayers.LonLat(coords[0], coords[1])
        this.olPoint.transform(this.mapPaneView.epsg4326Proj, this.mapPaneView.openLayersMap.getProjectionObject())

        var size = new OpenLayers.Size(this.model.iconSize[0], this.model.iconSize[1])
        var offset = new OpenLayers.Pixel(-(size.w/2), -size.h)
        var icon = new OpenLayers.Icon(this.model.iconUrl, size, offset)
        this.icon = icon
        var olMarker = new OpenLayers.Marker(this.olPoint, icon.clone())

        // TODO, move all these to event listeners on the view
        olMarker.events.register("click", olMarker, function(mouseEvent) {
            olMarkerView.model.trigger('click', olMarkerView.model)
            if(olMarkerView.model.click){
              olMarkerView.model.click(olMarkerView, mouseEvent)
            }
            if(olMarkerView.model.infoBubble){
              olMarkerView.openBubble()
            }        
          })

        this.olMarker = olMarker
        this.mapPaneView.markers.addMarker(this.olMarker)
        return this
    },

    openBubble: function() {
      // Need to create a new popup in case setInfoBubble has been called
      this.popup = new OpenLayers.Popup.FramedCloud(null,
          this.olPoint,
          new OpenLayers.Size(100,100),
          this.model.infoBubble(),
          this.icon,
          true
          );

      if ( this.popup ) {
        this.mapPaneView.openLayersMap.addPopup( this.popup, true );
        this.mapPaneView.openLayersMap._popup = this.popup
      }


    },

    closeBubble: function() {
      var popup = this.mapPaneView.openLayersMap._popup
      if (popup){
        popup.hide()
        this.mapPaneView.openLayersMap.removePopup(popup)
      }
    },


    remove:function(){

      this.proprietary_marker.display(false);

    }

  })

  webmapper.openlayers.PolygonView = webmapper.AbstractPolygonView.extend({

    render:function(){

      var polyView = this
      var polygon = this.model

      if(!this.mapPaneView.polyVectors){
        this.mapPaneView.polyVectors = new OpenLayers.Layer.Vector("Vector Layer")
        console.log(this.mapPaneView.polyVectors)
        this.mapPaneView.polyVectors.projection = this.mapPaneView.openLayersMap.getProjectionObject()
        this.mapPaneView.openLayersMap.addLayers([this.mapPaneView.polyVectors])
      }
      var jsonFormat = new OpenLayers.Format.GeoJSON({
                        'internalProjection': this.mapPaneView.openLayersMap.getProjectionObject(),
                        'externalProjection': this.mapPaneView.epsg4326Proj
                    })
      var feat = jsonFormat.read(polygon.get('geojson'))
      this.mapPaneView.polyVectors.addFeatures(feat)

      return this

    },
    remove:function(){
    }

  })


  // Widget specific views for webmapper.KmlLayer model
  // a little unconventional in that we are expressing the view
  // as a google object, not DOM els
  webmapper.openlayers.KmlView = webmapper.AbstractKmlView.extend({

    render: function(){
      if(this.openlayersKmlLayer){
        // can we remove it?
        this.openlayersKmlLayer.setMap(null)
      }
      else{
        this.openlayersKmlLayer = new OpenLayers.Layer.Vector("KML", {
          strategies: [new OpenLayers.Strategy.Fixed()],
          projection: new OpenLayers.Projection("EPSG:4326"),
          protocol: new OpenLayers.Protocol.HTTP({
            url: this.model.get('kml_url'),
            format: new OpenLayers.Format.KML({
              extractStyles: true,
              extractAttributes: true,
              maxDepth: 2
            })
          })
        })
        this.mapPaneView.openLayersMap.addLayer(this.openlayersKmlLayer)
      }
    },

    remove: function(){
      if(this.openlayersKmlLayer)
        this.openlayersKmlLayer.setMap(null)
      return this.openlayersKmlLayer
    }
  })

  return webmapper

})(window.webmapper || {});

