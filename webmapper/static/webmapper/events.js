window.webmapper = (function(webmapper){

  webmapper.events = {}
  webmapper.events.MapEvent = webmapper.BaseModel.extend()
  webmapper.events.MapClick = webmapper.events.MapEvent.extend({
  
    initialize:function(opts){
      this.set('lat', opts.lat)
      this.set('lon', opts.lon) 
      this.mapPane = opts.mapPane
    },

    toPoint: function(pointClass){
    
      var pointClass = pointClass || webmapper.Point
      var geojson = {}
      geojson.type = "Point"
      geojson.coordinates = [this.get('lon'), this.get('lat')]
      return point = new pointClass({
        geojson: geojson, 
        lon:this.get('lon'), 
        lat:this.get('lat')}) 
    }
  })

  return webmapper

})(window.webmapper || {});

