window.webmapper = (function(webmapper){

  webmapper.google = {}

  webmapper.google.MapPaneView = webmapper.AbstractMapPaneView.extend({

    renderMapWidget: function(){

      var googOpts = this.model.get('google_options')
      // override model scrolling config
      if(this.model.map.options.scrollZoom === false){
        googOpts.scrollwheel = false
      }

      this.googleMapsMap = new google.maps.Map(this.el, googOpts)

      var googleMapPaneView = this
      google.maps.event.addListener(this.googleMapsMap, 'tilesloaded', function(){ 
        googleMapPaneView.trigger('tiles-loaded') 
      })

      var coords = this.model.get('center').coordinates
      this.googleMapsMap.setCenter(new google.maps.LatLng(coords[1], coords[0]))


    },

    removeMapWidget: function(){
      if(this.googleMapsMap){
        this.googleMapsMap.unbindAll()
      }
      $(this.el).empty()
    },

    bindMapWidgetEvents: function(){

      _.bindAll(this, "bindMapWidgetEvents",'propagateCenter','propagateZoom', 'propagateMapTypeId')
      google.maps.event.addListener(this.googleMapsMap, 'zoom_changed', this.propagateZoom)
      google.maps.event.addListener(this.googleMapsMap, 'dragend', this.propagateCenter)
      google.maps.event.addListener(this.googleMapsMap, 'maptypeid_changed', this.propagateMapTypeId)

      // event abstraction providers
      google.maps.event.addListener(this.googleMapsMap, 
                                    'click', 
                                    this._eventAttrConstructor('click', this.processMapClick))

      _.bindAll(this)
      this.on('reflow', this.reflow)
    },

    syncMapWidget:function(){
      // synchronize the MapPane server state to the map widget 
      // honor the fillContainer option
      // TODO deprecate this function, we can keep this up to date using signals

      var google_options = this.model.get('google_options')
      var coords = this.model.get('center').coordinates

      this.googleMapsMap.setZoom(parseInt(this.model.get('zoom')))
      this.googleMapsMap.setMapTypeId(google_options.mapTypeId)
      this.googleMapsMap.setCenter(new google.maps.LatLng(coords[1], coords[0]))

      if(!this.model.map.options.fillContainer){
        $(this.googleMapsMap.getDiv()).css('height',this.model.get('height'))
        $(this.googleMapsMap.getDiv()).css('width',this.model.get('width'))
        google.maps.event.trigger(this.googleMapsMap, 'resize')
      }

    },

    attachKML:function(kmlCollection){
      var mapPaneView = this
      kmlCollection.each(function(kmlLayer){
        kmlLayer.view = new webmapper.google.KmlView({model: kmlLayer, 
                                                      mapPaneView:mapPaneView }) 
        kmlLayer.view.render()
      })
    },

    attachPoints:function(pointCollection){

      var mapPaneView = this
      var baseMarkerOptions = {}

      pointCollection.each(function(point){
        point.mapPane = mapPaneView.model
        point.markerView = new webmapper.google.MarkerView({model:point,
                                                            mapPaneView:mapPaneView})
        point.markerView.render()

      })
    },

    attachPolygons:function(polygonCollection){

      var mapPaneView = this

      polygonCollection.each(function(polygon){

        polygon.polygonView = new webmapper.google.PolygonView({model:polygon, 
                                                                mapPaneView:mapPaneView})
        polygon.polygonView.render()


      })
    //google.maps.PolygonOptions
    },

    getBounds:function(){

      var bounds = this.googleMapsMap.getBounds()
      var sw = bounds.getSouthWest()
      var ne = bounds.getNorthEast()
      return [sw.lng(), sw.lat(),ne.lng(), ne.lat()]

    },
    propagateCenter: function(){
      // from the widget to the model

        var gCntr = this.googleMapsMap.getCenter()
        // complex attributes need a new pointer for a change event to be 
        // registered so copy the attribute
        var newCenter = _.extend({}, this.model.get('center'))
        newCenter.coordinates = [gCntr.lng(),gCntr.lat()]
        this.model.set({center:newCenter})
    },

    propagateZoom: function(){
      // from the widget to the model
      this.model.set({zoom: this.googleMapsMap.getZoom()})
    },

    propagateMapTypeId: function(){
      // from the widget to the model
      var google_options = _.extend({}, this.model.get('google_options'))
      google_options.mapTypeId = this.googleMapsMap.getMapTypeId()
      this.model.set({ google_options: google_options })
    },

    getLayerId: function(){
      return this.googleMapsMap.getMapTypeId()
    },

    reflow:function(){
      // redraw the map upon container resize
      google.maps.event.trigger(this.googleMapsMap, 'resize')
    },

    paneDiv: function(){
      // return the principle map widget container el
      return this.googleMapsMap.getDiv()
    },

    // Events: functions to translate event objects 
 
    processMapClick:function(mouseEvent){
      return new webmapper.events.MapClick({lat:mouseEvent.latLng.lat(),
                                            lon:mouseEvent.latLng.lng(),
                                            mapPane: this.model,
                                            proprietary:mouseEvent})
    },

  })

  // Widget specific views for webmapper.KmlLayer model
  // a little unconventional in that we are expressing the view
  // as a google object, not DOM els
  webmapper.google.KmlView = webmapper.AbstractKmlView.extend({

    render: function(){
      if(this.googleKmlLayer){
        this.googleKmlLayer.setMap(null)
      }
      else{
        this.googleKmlLayer = new google.maps.KmlLayer(this.model.get('kml_url'), 
                                                       {preserveViewport:true})
        this.googleKmlLayer.setMap(this.mapPaneView.googleMapsMap)
      }
      return this
    },
    remove: function(){
      if(this.googleKmlLayer)
        this.googleKmlLayer.setMap(null)
      return this
    }

  })

  webmapper.google.MarkerView = webmapper.AbstractMarkerView.extend({


    googleMarker:null,

    render:function(){

        var googleMarkerView = this

        var googleMarkerOptions = {optimized:true}

        var coords = this.model.get('geojson').coordinates
        var googPoint = new google.maps.LatLng(coords[1], coords[0])

        googleMarkerOptions.position = googPoint

        if(this.model.click || this.model.infoBubble)
          googleMarkerOptions.clickable = true
        else
          googleMarkerOptions.clickable = false

        if(this.model.labelText)
          googleMarkerOptions.title = this.model.labelText()      

        if(this.model.iconUrl)
          googleMarkerOptions.icon = new google.maps.MarkerImage(
              this.model.iconUrl,
              new google.maps.Size(this.model.iconSize[0], this.model.iconSize[1]),
              // offset for sprite
              new google.maps.Point(0, 0),
              new google.maps.Point(this.model.imageAnchor[0], this.model.imageAnchor[1])
             // new google.maps.Size(this.model.iconScaledSize[0], this.model.iconScaledSize[1])
              );

        this.googleMarker = new google.maps.Marker(googleMarkerOptions)

        google.maps.event.addListener(this.googleMarker, 
                                      'click',
                                      function(mouseEvent){ 
                                        googleMarkerView.model.trigger('click', googleMarkerView.model)
                                        if(googleMarkerView.model.click){
                                          googleMarkerView.model.click(googleMarkerView, mouseEvent)
                                        }
                                        if(googleMarkerView.model.infoBubble){
                                          googleMarkerView.openBubble()
                                        }
                                      })


        this.googleMarker.setMap(this.mapPaneView.googleMapsMap)

        return this
    },

    openBubble:function(){

      var markerView = this
      this.closeBubble()

      //_.bindAll(markerView)
      var infowindow = new google.maps.InfoWindow({
        content: this.model.infoBubble()
      })
      /*google.maps.event.addListener(infowindow, 'closeclick', function(closedWindow) {
        markerView.closeBubble()
      })*/
      infowindow.open(this.mapPaneView.googleMapsMap, this.googleMarker)
      this.model.trigger('open-info-bubble', this.model)

      // store the info window "globally"
      // and ensure that only one is open at a time
      this.mapPaneView.googleMapsMap._infowindow = infowindow

    },

    closeBubble:function(){
      if (this.mapPaneView.googleMapsMap._infowindow) {
        this.mapPaneView.googleMapsMap._infowindow.close()
        this.mapPaneView.googleMapsMap._infowindow = null
        this.model.trigger('close-info-bubble', this.model)
      }
    },

    remove:function(){

      this.googleMarker.setMap(null)

    }

  })

  webmapper.google.PolygonView = webmapper.AbstractPolygonView.extend({

    render:function(){

      var polygon = this.model

      var googlePolygons = new google.maps.MVCArray()

      _.each(polygon.get('geojson').coordinates, function(poly){
        // XXXXXX, why is this nested?
        poly = poly[0]

        var singlePolygon = new google.maps.MVCArray()

        // web workers?
        for(var i=0; i<poly.length;i++){
          singlePolygon.push(new google.maps.LatLng(poly[i][1], poly[i][0]))
        }
        googlePolygons.push(singlePolygon)
      })
      var googlePolyset = new google.maps.Polygon({map:this.mapPaneView.googleMapsMap,
                                                   paths:googlePolygons, 
                                                   clickable:false,
                                                   strokeColor:this.color, 
                                                   fillOpacity:this.fillOpacity})
      polygon.googlePolyset = googlePolyset

    },
    remove:function(){
    }

  })

  return webmapper

})(window.webmapper || {});

