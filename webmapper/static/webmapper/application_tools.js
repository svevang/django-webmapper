window.webmapper = (function(webmapper){

  webmapper.tools = {}

  webmapper.tools.BasePaletteButtonView = Backbone.View.extend({

    tagName: 'div',
    className: 'tool-button',
    buttonText: 'button',

    initialize:function(opts){
      _.bindAll(this)
      this.buttonText = opts.buttonText || "button"
      //this.model.on(deactivate, this.deactivateButton)
    },

    events:{
      "click input":"toggleTool",
    },

    activateButton: function(){
      // XXX why cant I programatically toggle a button
      //this.$el.find('label').addClass('ui-state-active')
      //this.$el.find('label').attr('aria-pressed', 'true');
    },
    deactivateButton: function(){
      // XXX why cant I programatically toggle a button
      // remove active ui
      this.$el.find('label').removeClass('ui-state-active')
      this.$el.find('label').attr('aria-pressed', 'false');
    },

    buttonActive:function(){
      var classes = this.$el.find('label').attr('class').split(' ')
      if(_(classes).find(function(str){return str=='ui-state-active'}))
        return true
      return false
    },

    toggleTool:function(){
      console.log('calling toggle ----------------------------------------')
      console.log('activated?:', this.model.activated)
      // determine if we are activating or deactivating this tool
      if(this.model.activated){
        this.deactivateButton()
        this.model.deactivate()
        console.log('deactivating from toggle')
      }
      else{
        this.activateButton()
        this.model.activate()
        this.model.activated = true
        console.log('activating from toggle')
      }
    },

    render:function(){
      this.$el.empty()
      var uid = _.uniqueId('button_')
      this.$el.append($('<input type="radio" name="button"></input>')
              .attr('id', uid))
      this.$el.append($('<label></label>').attr('for', uid)
              .text(this.buttonText))
      this.$el.find('#'+uid)
              .button({icons: {primary:'ui-icon-gear'}, text:true})
      this.$el.buttonset()
      return this
    }

  })

  /** Tools **/

  webmapper.tools.BaseTool = webmapper.BaseModel.extend({

    // The Base Tool api
    toolButtonUrl: null,
    paletteButtonView: null,
    activated:false,

    initialize:function(){
      _(this).bindAll() 
      this.uid = _.uniqueId()
    },

    bindMap:function(map){
      var tool = this
      this.map = map
      this.map.on('init-ready', function(){ 
        tool.delegate(map)
      })
    },

    activate:function(){
      console.log('activate tool', this)
      this.activated = true
      this.trigger('activate', this)
      if(this.secondaryInfoWindow){
        $('#tools.webmapper-ns').append(this.secondaryInfoWindow.render().el)
      }
      this.buildUp()
      
    },

    deactivate:function(){
      console.log('deactivate tool', this)
      this.activated = false
      this.trigger('deactivate', this)
      if(this.secondaryInfoWindow){
        this.secondaryInfoWindow.remove()
      }
      this.tearDown()
    },

    buildUp: function(){
    },

    tearDown: function(){ 
    },

    delegate:function(map){
      // unbind or bind to map event handlers here
      
      // declare a button view attr to register in the tool palette
      this.paletteButtonView = null
    }

  })

  webmapper.tools.SecondaryInfoWindow = Backbone.View.extend({

    // provide a secondary view for a tool mode
    // when tools are engaged, they can display secondary information
    // alongside the map
    
    tagName: "div",
    className: "info-window interface-box",

    render: function(){
      return this
    }
  })

  webmapper.tools.DebugWindow = webmapper.tools.SecondaryInfoWindow.extend({
    initialize:function(){
      _.bindAll(this)
      this.model.on('mapClick', this.log)  
    },
    log:function(clickEvent){
      var message = 'Lat: ' + clickEvent.get('lat') + '\nLon: ' + clickEvent.get('lon')

      this.$el.html($("<pre>"+message+"</pre>"))
    }
  })

  webmapper.tools.FileUploadWindow = webmapper.tools.SecondaryInfoWindow.extend({

    // provide a secondary view for a tool mode
    // when tools are engaged, they can display secondary information
    // alongside the map
    
    tagName: "div",
    className: "info-window interface-box",
    uploadFormTemplate: null,
    uploadFormOpts:{},
    events: {
      "click .dismiss": "dismiss",
      "click .upload": "commenceUpload",
    },

    initialize:function(opts, uploaderOpts){
      // pass uploader options through
      this.uploaderOpts = uploaderOpts || {}
      this.uploaderOpts.url = this.uploaderOpts.url || ""
      this.uploaderOpts.type = this.uploaderOpts.type || "get" 
      
      // an exception allow extra objs to be passed through as a convenience
      // uploader expects any extra data to be formatted for easy conversion to
      // multipart/form-data
      this.uploaderOpts.formData = this.objectToFormData(opts.extraData)

      this.message = opts.message || "Select File" 

      if( typeof opts.confirmUpload == "undefined")
        this.confirmUpload = true
      else
        this.confirmUpload = opts.confirmUpload
      
      // pass this to the file upload widget form
      opts.selectSingle = opts.selectSingle ? true : false
      this.opts = opts 

      _.bindAll(this)
    },

    render: function(){
      console.log('in render!')
      this.$el.html('')
      var fileUploadWindow = this

      if(this.uploadFormTemplate)
        var uploadForm = webmapperConstant['templates'][this.uploadFormTemplate]
      else
        var uploadForm = _.template("<form></form>") 
      
      
      var templateOptions = _({action: this.action,
                                method: this.method,
                                message: this.message
                              }).extend(this.uploadFormOpts)

      var formEl = $(uploadForm(templateOptions))

      var fileSelectEl = $(webmapperConstant['templates']['file-upload-form-widget'](this.opts))

      fileSelectEl.button()
      if(!this.opts.selectSingle){
        $(fileSelectEl).find('input').attr('multiple','multiple')
      }

      var baseFileUploadOpts = {

          dataType: 'json',
          //multipart: false,

          limitConcurrentUploads: 2,

          //singleFileUploads: false,

          add:function(e, data){
            data.uid = _.uniqueId()
            data.view = new webmapper.tools.FileProgressBar({model:data}) 
            fileUploadWindow.$el.find('.fileinput-button').hide()

            // set up some backreferences and render
            data.fileUploadWindow = fileUploadWindow
            data.view.render()

            var existingProgressBars = fileUploadWindow.$el.find('.progress-bar-container')
            if(existingProgressBars.length > 0){
              //fileUploadWindow.$el.append(data.view.el)
              data.view.$el.insertAfter($(existingProgressBars).last())
              //$(existingProgressBars).last().after(data.view.el)
            }
            else{
              fileUploadWindow.$el.append(data.view.el)
            }

            data.fileUploadWindow.pushUploadQueue(data)
            if(fileUploadWindow.confirmUpload){
              // append a confirm button
              // check if one exists first
              if(fileUploadWindow.$el.find('.upload').length == 0){
                var confirmButton = fileUploadWindow.buildButton('upload', 'Upload')
                fileUploadWindow.$el.append(confirmButton)
              }
            }
            else{
              data.submit()
              fileUploadWindow.$el.find('.upload').remove()
            }
          },

          done:function(e, data){

           data.view.success()
           data.fileUploadWindow.popUploadQueue(data)
           var newKmls = _(data.result).map(function(kml_layer_json){
             return new webmapper.KmlLayer(kml_layer_json)
           })
           fileUploadWindow.model.map.mapPane.view.attachKML({models:newKmls})

          },

          fail:function(e, data){

            var receivedErrors = $.parseJSON(data.jqXHR.responseText)
            data.view.error(receivedErrors.errors)
            console.log('fail: ', receivedErrors.errors)
            data.fileUploadWindow.errors = true
            data.fileUploadWindow.popUploadQueue(data)

          },

          progress:function(e, data){
            data.view.updateProgress(parseInt(data.loaded / data.total * 100, 10))
          }
          
        }


      this.$el.html(formEl)
      this.$el.append(fileSelectEl)
      $(fileSelectEl).fileupload(_(baseFileUploadOpts).extend(this.uploaderOpts))
      this.fileSelectEl = fileSelectEl
      return this
    },

    /* a simple queue to keep track of uploads */
    pushUploadQueue: function(data){
      if(!this.uploadQueue){
        this.uploadQueue = []
      }
      this.uploadQueue.push(data)
      console.log(this.uploadQueue.length)
    },

    popUploadQueue: function(data){
      var finishedData = data
      this.uploadQueue = _(this.uploadQueue).filter(function(data){
        return data.uid != finishedData.uid
      })
      console.log(this.uploadQueue.length)
      if(this.uploadQueue.length == 0){
        // all uploads are done
        if(!this.errors){
          console.log('no errors!', this.errors)
          var jsonResp = JSON.parse(data.jqXHR.responseText)
          this.trigger('uploadsComplete', jsonResp)
          if(this.complete){
            this.complete(jsonResp)
          }
          this.render()
        }
        else{
          console.log('errors!', this.errors)
          this.$el.append(this.buildButton('dismiss','Dismiss'))
          // reset error state
          this.errors = false
        }
      }
    },

    dismiss: function(){
      this.trigger('dismiss')
      return this.render()
    },
    
    objectToFormData: function(obj){
    
      if(!obj)
        return []
      var formDataKeys = _(obj).keys()
      var formData = []
      for(var i=0; i< formDataKeys.length; i++){
        var key = formDataKeys[i]
        formData.push({
                            name: key,
                            value: obj[key]
                          }) 
      }
      return formData
    },

    buildButton:function(className, label){
      var buttonEl = $('<button class="'+ className +'" >'+label+'</button>')
      buttonEl.button()
      return buttonEl
    },

    commenceUpload: function(){

      this.trigger('commenceUpload')
      // get a reference to fileupload el and pull out the existing form data
      var formData = this.$el.find('.fileinput-button')
                             .fileupload('option','formData')
      
      // subclasses can include extra form data if they add inputs to the form
      // that is included in this.el
      var newFormData = this.$el.find('form').serializeArray()
      for(var i=0; i<newFormData.length; i++){
        formData.push(newFormData[i])
      } 
      // now reset the formData
      this.$el.find('.fileinput-button')
              .fileupload('option','formData', formData)

      _(this.uploadQueue).each(function(data){
        data.submit()
      })
      this.$el.find('.upload').remove()
    }

  })

  webmapper.tools.FileProgressBar = Backbone.View.extend({
    className: "progress-bar-container",
    initialize: function(){
      _.bindAll(this)
    },
    updateProgress: function(progress){
      console.log('Progress:', progress)
      this.$el.find('.progress-bar').progressbar("value", progress)
    },
    render: function(){
      this.$el.append(this.model.files[0].name)
      this.$el.append($('<div class="progress-bar"/>'))
      this.$el.find('.progress-bar').progressbar({value:0})
      return this
    },
    success: function(){
      var filename = this.model.files[0].name
      this.message(filename, {upload: ["Success!"]})

    },
    error: function(errorObj){

      var filename = this.model.files[0].name
      this.message(filename, errorObj)
      this.$el.find('.message').addClass('error')
    
    },
    message: function(filename, msgObj){
      console.log('message', filename, msgObj)
      this.$el.empty()
      fileProgressView = this

      var messageTpl = _.template('<p class="webmapper-ns"><%= filename %>:<br/><span class="message"><%= key %>:<br/><%= message %></span></p>')

      console.log(msgObj)
      _(msgObj).chain()
        .keys()
        .each(function(key){
          console.log(msgObj[key])
          fileProgressView.$el.append(messageTpl({
            message: msgObj[key], 
            filename: filename,
            key: key}
            ))
        })
    }


  })

  /* Test Tool */

  webmapper.tools.TestTool = webmapper.tools.BaseTool.extend({

    delegate:function(map){
      _.bindAll(this)
      // unbind or bind to map event handlers here
      this.map = map
      this.paletteButtonView = new webmapper.tools.TestToolButtonView({uid:this.uid, 
                                                                       model:this})
      this.infoWindow = new webmapper.tools.TestInfoWindow({model:this})
    },

    buildUp:function(){
      // turn this tool on
      $('#tools.webmapper-ns').append(this.infoWindow.render().el)
      this.infoWindow.updateInfoWindow()
      this.map.on('change', this.update)
      this.map.mapPane.on('change', this.update)
      this.infoWindow.updateInfoWindow('test tool')
    },

    tearDown:function(){
      this.infoWindow.remove()
      this.map.off('all', this.update)
      this.map.mapPane.off('all', this.update)
    },

    update:function(ev){
      var attrs = ev.changedAttributes()
      var el = $('<div></div>')
      _.each(_.zip(_.keys(attrs), _.values(attrs)), function(pair){
        el.append($('<p>'+ pair[0] + ':' + pair[1] +'</p>'))
      })
      this.infoWindow.updateInfoWindow(el)
    },
  })

  webmapper.tools.TestInfoWindow = webmapper.tools.SecondaryInfoWindow.extend({
    updateInfoWindow : function(el){
      this.$el.html(el)
    }, 
  })

  webmapper.tools.TestToolButtonView = webmapper.tools.BasePaletteButtonView.extend({
    initialize:function(opts){
      this.buttonText = "Test Tool" 
    }
  })


  /* Kml Tool */

  webmapper.tools.KmlTool =  webmapper.tools.BaseTool.extend({

    toolDescr:"Kml Tool",

    delegate:function(map){
      // unbind or bind to map event handlers here
      this.map = map
      // express this tool as a button in the tool palette
      this.paletteButtonView = new webmapper.tools.KmlToolButtonView({model:this})
      // a modal window that is built and destroyed as the palette button is engaged
      var uploadUrl = webmapperConstant['api_kml_layers_url'] + '?map_id='+map.id
      this.secondaryInfoWindow = new webmapper.tools.FileUploadWindow({model:this},
                                                                      {url: uploadUrl,
                                                                       type: "post"})
    },

    buildUp:function(){
      console.log('KmlTool Selected!')
    },

    tearDown:function(){
      console.log('KmlTool DeSelected!')
    }

  })

  webmapper.tools.KmlToolButtonView = webmapper.tools.BasePaletteButtonView.extend({
    initialize:function(opts){
      this.buttonText = "KML upload"
    }
  })


  /* Marker Tool */

  webmapper.tools.MarkerTool = webmapper.tools.BaseTool.extend({

    // create a point and annotate them on a map

    toolButtonUrl:null,
    toolDescr:"Marker Tool",
    modalCursor: 'crosshair',
    
    delegate:function(map){

      var markerTool = this
      this.map = map
      this.paletteButtonView = new webmapper.tools.BasePaletteButtonView({model:this, buttonText:this.toolDescr})
      this.secondaryInfoWindow = new webmapper.tools.DebugWindow({model:this})
      this.clickDialog = new webmapper.tools.ClickDialog()
      
    },

    buildUp:function(){
      var markerTool = this
      this.map.mapPane.view.on('click', this.processMapClick) 
      $(this.map.mapPane.view.paneDiv()).css('cursor', this.modalCursor)
    },

    tearDown:function(){
      this.map.mapPane.view.off('click', this.processMapClick) 
      $(this.map.mapPane.view.paneDiv()).css('cursor','auto')
    },

    processMapClick:function(clickEvent){
      console.log('marker tool click')
      // re-trigger this event, now on the MarkerTool model
      this.clickDialog.render()
      var point = new webmapper.Point({geojson:{type:"Point", coordinates:[clickEvent.get('lon'), 
                                                                            clickEvent.get('lat')]}})
      this.trigger('mapPaneClick', clickEvent, this.clickDialog, point)
    }

  })
  webmapper.tools.ClickDialog = Backbone.View.extend({
        initialize: function(){
          this.$el.dialog({ modal: true, 
                            autoOpen: false,
                            draggable: false,
                            position: 'top' })
          var clickDialog = this
          this.$el.dialog({
            open:function(event, ui){
              markerTool = new webmapper.tools.FileUploadWindow()
              console.log('photoupload: dialog opened')
            },
            close: function(event, ui){
              console.log('photoupload: dialog closed')

            },
            beforeClose: function(event, ui){
              console.log('photoupload: dialog beforeclosed')
            },
          })

        },
        render:function(){
          this.$el.dialog("open")
          return this
        },
      
      })

  return webmapper

})(window.webmapper || {});
