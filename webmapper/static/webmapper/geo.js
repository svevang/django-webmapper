/* geo.js */
window.webmapper = (function(webmapper){

  webmapper.BaseGeoModel = webmapper.BaseModel.extend({

    initialize:function(){
      // XXX
      // we expect the geom to be JSON encoded (from postgis geojson)
      if(!this.isNew()){
        var jsonObj = JSON.parse(this.get('geojson'))
        this.set({geojson: jsonObj}, {silent:true})
       }
    }

  })

  webmapper.Point = webmapper.BaseGeoModel.extend({

    // boolean
    draggable:null,
    iconUrl: webmapperConstant['static_url'] + 'webmapper/images/red-dot.png',

    // implement these as functions
    labelText:null,
    click:null,
    infoBubble:null,

    // tuple, [x, y]
    iconSize:[32, 32],
    imageAnchor:[16, 32],

    urlRoot: webmapperConstant['api_points_url']

  })

  webmapper.Polyline = webmapper.BaseGeoModel.extend({

    initialize:function(opts){

      webmapper.BaseGeoModel.prototype.initialize.call(opts)

      this.color = opts.color || '#333'

    },
    
    urlRoot: webmapperConstant['api_polylines_url']


  })

  webmapper.Polygon = webmapper.Polyline.extend({

    initialize:function(opts){

      webmapper.Polyline.prototype.initialize.call(opts)

      this.fillColor = opts.fillColor || '#333'
      this.fillOpacity = opts.fillOpacity || 0.2
      this.closed = true
    },
    urlRoot: webmapperConstant['api_polygons_url']
    
  })

  webmapper.KmlLayer = webmapper.BaseModel.extend({

    urlRoot: webmapperConstant['api_kml_layers_url'],

  })

  webmapper.BaseGeoCollection = webmapper.BaseCollection.extend({

    model: webmapper.BaseGeoModel,

    initialize:function(models, args){
      this.map = args.map
      this.setApiParam('map_id', this.map.id)

      var baseGeoCollection = this
      _(models).each(function(model){
        baseGeoCollection.add(model)
      })

    },

  })

  webmapper.KmlLayerCollection = webmapper.BaseGeoCollection.extend({

    model: webmapper.KmlLayer,

    urlRoot: webmapperConstant['api_kml_layers_url']

  })


  webmapper.PointCollection = webmapper.BaseGeoCollection.extend({

    model:webmapper.Point,

    urlRoot: webmapperConstant['api_points_url']

  })

  webmapper.PolygonCollection = webmapper.BaseGeoCollection.extend({

    urlRoot: webmapperConstant['api_polygons_url']

  })

  /*
   * Views
   */

  webmapper.BaseGeoView = Backbone.View.extend({
    initialize:function(opts){
      this.mapPaneView = opts.mapPaneView
    }

  })

  webmapper.AbstractMarkerView = webmapper.BaseGeoView.extend({
    // define a abstract view for map implementations
    // of MarkerViews (Point displayed on a map)

    // add-marker event
    render:function(){throw('Not Implemented')},

    // remove-marker event
    remove:function(){throw('Not Implemented')},

    // open-info-window event
    openBubble:function(){throw('Not Implemented')},

    // close-info-window event
    closeBubble:function(){throw('Not Implemented')}

  })

  webmapper.AbstractPolygonView = webmapper.BaseGeoView.extend({

  })

  webmapper.AbstractKmlView = webmapper.BaseGeoView.extend({

  })

  return webmapper
})(window.webmapper || {});
