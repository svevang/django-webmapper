window.webmapper = (function(webmapper){

  webmapper.BaseApplicationView = Backbone.View.extend({

    bindMap:function(map){
      var view = this
      this.map = map
      map.on('init-ready', function(){ 
        view.delegate(map)
        view.render()
      })
    },

    delegate: function(map){
    }
  })

  /** Palettes **/
  webmapper.ToolPaletteView = webmapper.BaseApplicationView.extend({

    tagName: "div",
    className: "tool-palette edit-region",
    toolPaletteTemplate: null,

    delegate:function(map){
      this.map = map
      this.model = map

      _.bindAll(this)
      this.map.mapPane.on('change:map_type reset', this.deactivateAllTools) 

    },

    activateTool:function(tool){

      var toolPaletteView = this

      var otherTools = _(this.map.tools).filter(function(someTool){
        return !(someTool.uid==tool.uid)
      }) 
      _(otherTools).each(function(otherTool){
          if(otherTool.activated){
          otherTool.deactivate()
        }
      })
    },

    deactivateTool:function(tool){
    },

    deactivateAllTools:function(tool){
      var toolPaletteView = this 
      _(this.map.tools).map(function(tool){
          tool.deactivate()
      })
      this.$el.find('label').removeClass('ui-state-active')
    },

    render: function(){

      if(!this.toolPaletteTemplate)
        this.toolPaletteTemplate = this.model.getTemplate("tool-palette")

      // closure
      var toolPaletteView = this

      this.$el.html('')
      this.$el.append($('<pre>Tools:</pre>'))
      var toolPaletteView = this

      /* inspect webmapper tools, 
       * register those tools that can characterize a palette interface */

      _.each(this.model.tools, function(tool){
        if(tool.paletteButtonView){
          // create a reference in each button view to the palette view
          tool.paletteButtonView.toolPaletteView = toolPaletteView;

          toolPaletteView.$el.append(tool.paletteButtonView.render().el)
          tool.on('activate', function(){ 
            toolPaletteView.activateTool(tool) 
          })
          tool.on('deactivate', function(){ 
            toolPaletteView.deactivateTool(tool) 
          })

        }
      })
      return this
    }
  })

  webmapper.TitleDescriptionView = webmapper.BaseApplicationView.extend({
    // A simple aplication view that allows editing of the Map's title attr

    id:"title-description-config",
    className:"interface-box",

    initialize:function(opts){
      this.editable = opts.editable || false
      this.saveEdit = _.debounce(this.saveEdit, 2000)
    },

    delegate:function(map){
      this.map = map 
      this.template = map.getTemplate('description')
    },
   
    events:{
      'click .editable': "doEdit"
    },

    render:function(){
      var json = this.map.toJSON()
      json = _.extend(json, {'editable':this.editable})
      
      this.$el.html(this.template(json))

      this.nullifyLinks(this.$el.find('.editable'));
      
      //this.$el.children('.editable').dialog({ autoOpen: false })
      this.confirmInput = this.$el.find('.modal-confirmation').detach()

      return this
    },

    doEdit:function(eventData){

      var titleDescriptionView = this
      var target = $(eventData.target)
      while(!$(target).hasClass('editable')){
        target = $(target).parent()
      }
      var clone = this.confirmInput.clone().dialog({ 
        autoOpen: true,
        title: "Change: " + $(target).attr('name'),
        modal: true,
        minHeight:300,
        minWidth: 400,
        resizable: true,
        buttons: [{
                    text: "Ok",
                    click: function() { 

                      $(this).dialog("close"); 
                      
                      var changedKey = $(target).attr('name')
                      var changedValue = $(this).find('[name="new value"]').val()
                      $(target).html(changedValue)

                      var update = {}
                      update[changedKey] = changedValue
                      titleDescriptionView.map.set(update)
                      titleDescriptionView.nullifyLinks(titleDescriptionView.$el);
                    }
                  },
                  {
                    text: "Cancel",
                    click: function() { $(this).dialog("close"); }
                  } 
                 ]      
      })
      clone.find('[name="new value"]')
        .replaceWith('<textarea style="width:90%; height:290px;" name="new value"></textarea>')
      clone.find('[name="new value"]').text($(target).html())
      
    },
    saveEdit:function(){
      var changedKey = $(eventData.target).attr('name')
      var changedValue = $(eventData.target).html()
      
      var update = {}
      update[changedKey] = changedValue
      this.map.set(update)
      this.map.save()

    },

    nullifyLinks: function(el){
      var view = this
      $(el).find('a').click(function(e){ 
        e.preventDefault()
        view.doEdit(e);
        return false
      })
      console.log($(el).find('a'))
    }
  })

  webmapper.DeleteMapView = webmapper.BaseApplicationView.extend({

    id:"delete-config",

    className:"interface-box",

    delegate:function(map){
      this.map = map 
    },

    render:function(){
      this.$el.html($('<input type="button" value="\u2327 Delete This Map" />'))
    },

    events:{
      'click input': "deleteMap"
    },

    deleteMap:function(){
      if(confirm('Delete this map? The cannot be undone.')){
        $.when(this.map.destroy()).done(function(){
          window.location = ".."
        })
      }
    }
  })

  webmapper.MapCoreEditView = webmapper.BaseApplicationView.extend({

    tagName: "div",
    className: "edit-region core-edit",
    coreEditTemplate: null,

    delegate: function(map){
     console.log('in application delegate', map)
     var coreEditView = this
     this.map = map
     this.map.mapPane.on('change', function(){ coreEditView.render() } )

    },

    events: {
         "click .reset": "reset",
         "click .save": "submit",
         "click :radio": "changeMapWidget"
    },

    changeMapWidget: function(eventData){
      var mapPane = this.map.mapPane
      mapPane.changeMapWidget(eventData.target.value)
    },

    submit: function(){
      var mapPane = this.map.mapPane
      dims = {
          width: parseInt($(this.el).find('[name="width"]').val()),
          height: parseInt($(this.el).find('[name="height"]').val())
      }
      mapPane.set(dims, {silent: false})
      console.log(mapPane)
      $.when(mapPane.save()).done(function(){
        console.log('calling syncMapWidget')
        mapPane.view.syncMapWidget()
      })
    },

    reset: function(){
      var mapPane = this.map.mapPane
      var coreEdit = this

      $.when(mapPane.fetch()).done(function(){
          mapPane.view.syncMapWidget()
          var preservedMapType = $(coreEdit.el).find(':radio:checked').attr('value')
          // TODO: dont reset the map widget type unless it is different
          // Mock the jquery unified event object
          coreEdit.changeMapWidget({target:{value:preservedMapType}})
      })
      mapPane.trigger('reset') 

    },

    render: function(){

      var coreEditView = this

      // as an applicationView we only have access to model in render
      if(!this.coreEditTemplate)
        this.coreEditTemplate = this.map.getTemplate("core-edit")
      // when mapPane is changed, update core edit fields display
      var attrs = this.map.mapPane.toJSON()
      attrs.layerId = this.map.mapPane.view.getLayerId()
      $(this.el).html(this.coreEditTemplate(attrs))

     // HACK, trigger the save dialog when ever we enter values here
     function changeMap(){
      var mapPane = coreEditView.map.mapPane
      dims = {
          width: parseInt($(coreEditView.el).find('[name="width"]').val()),
          height: parseInt($(coreEditView.el).find('[name="height"]').val())
      }
      mapPane.set(dims, {silent: false})
      mapPane.view.reflowSize()
      mapPane.view.syncMapWidget()
     }
     this.$el.find('[name="width"]').bind('change',changeMap)
     this.$el.find('[name="height"]').bind('change',changeMap)
     // end HACK

     return this

    }
  })

  webmapper.StatusDisplay = webmapper.BaseApplicationView.extend({

    id:"status-display",
    tagName:"div",

    delegate:function(map){
      this.map = map
      this.map.statusDisplay = this
      var statusDisplay = this

      _.bindAll(this)

      this.map.on('loaded', function(){
        $('#webmapper-container').append(statusDisplay.render().$el)
        console.log(statusDisplay.el)
      })
    },

    status:function(message){

      if(message){
          var p = $('<p></p>')
          p.html(message)
          this.$el.html(p)
          this.$el.children().fadeOut(2000)
      }
    },
  })

  webmapper.SaveView = webmapper.BaseApplicationView.extend({

    id:"save-changes-config",

    className:"interface-box",

    initialize:function(opts){
      // TODO break this template out
      this.tpl = _.template('<h3>Your map has changed:</h3><input type="button" value="save" id="save-changes-button"><input type="button" value="reset" id="reset-changes-button">')
      this.container = opts.container
    },

    delegate:function(map){
      this.map = map
      this.map.on('change',this.display, this)
      this.map.mapPane.on('change',this.display, this)
      this.$el.html(this.tpl({}))
    },

    display:function(){
      if(!this.isDisplayed())
        $(this.container).append(this.render().el)
    },

    render:function(){
      // hack to redelegate events upon 'remove' from DOM
      this.setElement(this.el)
      return this
    },

    events:{
      'click #save-changes-button': 'saveMap',
      'click #reset-changes-button': 'resetMap',
    },

    resetMap:function(){
      var saveView = this
      $.when(this.map.fetch(), this.map.mapPane.fetch()).done(function(){
        saveView.map.mapPane.view.syncMapWidget()
        saveView.map.statusDisplay.status('reset')
        saveView.remove()
      })
    },

    saveMap:function(){
      var saveView = this
      $.when(this.map.save(),this.map.mapPane.save()).done(function(){
        saveView.map.statusDisplay.status('saved')
        saveView.remove()
      })
    },

    isDisplayed:function(){
      return $('#'+this.id).length && true
    },

  })

  return webmapper

})(window.webmapper || {});

