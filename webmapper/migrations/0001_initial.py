# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding model 'Map'
        db.create_table('webmapper_map', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('slug', self.gf('django.db.models.fields.CharField')(unique=True, max_length=128)),
            ('description', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal('webmapper', ['Map'])

        # Adding model 'MapPane'
        db.create_table('webmapper_mappane', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('default', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('slug', self.gf('django.db.models.fields.CharField')(unique=True, max_length=128)),
            ('description', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('map', self.gf('django.db.models.fields.related.ForeignKey')(related_name='map_panes', to=orm['webmapper.Map'])),
            ('center', self.gf('django.contrib.gis.db.models.fields.PointField')(default='POINT(-103.852729 44.674512)')),
            ('zoom', self.gf('django.db.models.fields.DecimalField')(default=3, max_digits=2, decimal_places=0)),
            ('height', self.gf('django.db.models.fields.PositiveIntegerField')(default=400)),
            ('width', self.gf('django.db.models.fields.PositiveIntegerField')(default=600)),
            ('map_type', self.gf('django.db.models.fields.CharField')(default='google', max_length=64)),
            ('map_key', self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True, blank=True)),
        ))
        db.send_create_signal('webmapper', ['MapPane'])

        # Adding M2M table for field kml_layers on 'MapPane'
        db.create_table('webmapper_mappane_kml_layers', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('mappane', models.ForeignKey(orm['webmapper.mappane'], null=False)),
            ('kmllayer', models.ForeignKey(orm['webmapper.kmllayer'], null=False))
        ))
        db.create_unique('webmapper_mappane_kml_layers', ['mappane_id', 'kmllayer_id'])

        # Adding M2M table for field layers on 'MapPane'
        db.create_table('webmapper_mappane_layers', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('mappane', models.ForeignKey(orm['webmapper.mappane'], null=False)),
            ('layer', models.ForeignKey(orm['webmapper.layer'], null=False))
        ))
        db.create_unique('webmapper_mappane_layers', ['mappane_id', 'layer_id'])

        # Adding model 'KMLLayer'
        db.create_table('webmapper_kmllayer', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('description', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('map', self.gf('django.db.models.fields.related.ForeignKey')(related_name='kml_layers', to=orm['webmapper.Map'])),
            ('kml', self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True, blank=True)),
            ('kml_url', self.gf('django.db.models.fields.CharField')(max_length=2048, null=True, blank=True)),
            ('revision', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
        ))
        db.send_create_signal('webmapper', ['KMLLayer'])

        # Adding model 'Layer'
        db.create_table('webmapper_layer', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('description', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('map', self.gf('django.db.models.fields.related.ForeignKey')(related_name='layers', to=orm['webmapper.Map'])),
            ('revision', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
        ))
        db.send_create_signal('webmapper', ['Layer'])

        # Adding model 'Point'
        db.create_table('webmapper_point', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('point', self.gf('django.contrib.gis.db.models.fields.PointField')()),
            ('layer', self.gf('django.db.models.fields.related.ForeignKey')(related_name='points', to=orm['webmapper.Layer'])),
            ('label', self.gf('django.db.models.fields.CharField')(max_length=1024, null=True, blank=True)),
            ('attributes', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('html_repr', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal('webmapper', ['Point'])

        # Adding model 'PolyLine'
        db.create_table('webmapper_polyline', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('poly_line', self.gf('django.contrib.gis.db.models.fields.LineStringField')()),
            ('layer', self.gf('django.db.models.fields.related.ForeignKey')(related_name='polylines', to=orm['webmapper.Layer'])),
            ('label', self.gf('django.db.models.fields.CharField')(max_length=1024, null=True, blank=True)),
            ('attributes', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('html_repr', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal('webmapper', ['PolyLine'])

        # Adding model 'Polygon'
        db.create_table('webmapper_polygon', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('polygon', self.gf('django.contrib.gis.db.models.fields.PolygonField')()),
            ('layer', self.gf('django.db.models.fields.related.ForeignKey')(related_name='polygons', to=orm['webmapper.Layer'])),
            ('label', self.gf('django.db.models.fields.CharField')(max_length=1024, null=True, blank=True)),
            ('attributes', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('html_repr', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal('webmapper', ['Polygon'])

        # Adding model 'DjangoModelGeoItemEdge'
        db.create_table('webmapper_djangomodelgeoitemedge', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('geo_content_type', self.gf('django.db.models.fields.related.ForeignKey')(related_name='geo_edges', to=orm['contenttypes.ContentType'])),
            ('geo_object_id', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('content_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contenttypes.ContentType'])),
            ('object_id', self.gf('django.db.models.fields.PositiveIntegerField')()),
        ))
        db.send_create_signal('webmapper', ['DjangoModelGeoItemEdge'])

        # Adding model 'GoogleMapOptionSet'
        db.create_table('webmapper_googlemapoptionset', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('map_pane', self.gf('django.db.models.fields.related.OneToOneField')(related_name='google_map_options', unique=True, to=orm['webmapper.MapPane'])),
            ('disable_default_ui', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('disable_double_click_zoom', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('draggable', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('map_type_control', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('map_type', self.gf('django.db.models.fields.CharField')(default='TERRAIN', max_length=16)),
            ('no_clear', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('scale_control', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('scrollwheel', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal('webmapper', ['GoogleMapOptionSet'])

        # Adding model 'GPX'
        db.create_table('webmapper_gpx', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('gpx_file', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
        ))
        db.send_create_signal('webmapper', ['GPX'])

        # Adding model 'GPXTrack'
        db.create_table('webmapper_gpxtrack', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=1024, blank=True)),
            ('gpx', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['webmapper.GPX'])),
            ('track', self.gf('django.contrib.gis.db.models.fields.MultiLineStringField')()),
        ))
        db.send_create_signal('webmapper', ['GPXTrack'])

        # Adding model 'GPXSegment'
        db.create_table('webmapper_gpxsegment', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('gpx', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['webmapper.GPX'])),
            ('gpx_track', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['webmapper.GPXTrack'])),
            ('segment', self.gf('django.contrib.gis.db.models.fields.LineStringField')()),
        ))
        db.send_create_signal('webmapper', ['GPXSegment'])

        # Adding model 'GPXPoint'
        db.create_table('webmapper_gpxpoint', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('gpx', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['webmapper.GPX'])),
            ('gpx_track', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['webmapper.GPXTrack'], null=True, blank=True)),
            ('gpx_segment', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['webmapper.GPXSegment'], null=True, blank=True)),
            ('time', self.gf('django.db.models.fields.DateTimeField')()),
            ('point', self.gf('django.contrib.gis.db.models.fields.PointField')()),
        ))
        db.send_create_signal('webmapper', ['GPXPoint'])

        # Adding model 'GeoPhoto'
        db.create_table('webmapper_geophoto', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('point', self.gf('django.contrib.gis.db.models.fields.PointField')(null=True, blank=True)),
            ('time', self.gf('django.db.models.fields.DateTimeField')()),
            ('offset_time', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('gpx', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['webmapper.GPX'])),
            ('content_type_id', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('object_id', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('photo_field', self.gf('django.db.models.fields.CharField')(max_length=1024)),
        ))
        db.send_create_signal('webmapper', ['GeoPhoto'])

        # Adding model 'AerialBoundary'
        db.create_table('webmapper_aerialboundary', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('bounds', self.gf('django.contrib.gis.db.models.fields.PolygonField')()),
            ('geophoto', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['webmapper.GeoPhoto'], unique=True)),
        ))
        db.send_create_signal('webmapper', ['AerialBoundary'])


    def backwards(self, orm):
        
        # Deleting model 'Map'
        db.delete_table('webmapper_map')

        # Deleting model 'MapPane'
        db.delete_table('webmapper_mappane')

        # Removing M2M table for field kml_layers on 'MapPane'
        db.delete_table('webmapper_mappane_kml_layers')

        # Removing M2M table for field layers on 'MapPane'
        db.delete_table('webmapper_mappane_layers')

        # Deleting model 'KMLLayer'
        db.delete_table('webmapper_kmllayer')

        # Deleting model 'Layer'
        db.delete_table('webmapper_layer')

        # Deleting model 'Point'
        db.delete_table('webmapper_point')

        # Deleting model 'PolyLine'
        db.delete_table('webmapper_polyline')

        # Deleting model 'Polygon'
        db.delete_table('webmapper_polygon')

        # Deleting model 'DjangoModelGeoItemEdge'
        db.delete_table('webmapper_djangomodelgeoitemedge')

        # Deleting model 'GoogleMapOptionSet'
        db.delete_table('webmapper_googlemapoptionset')

        # Deleting model 'GPX'
        db.delete_table('webmapper_gpx')

        # Deleting model 'GPXTrack'
        db.delete_table('webmapper_gpxtrack')

        # Deleting model 'GPXSegment'
        db.delete_table('webmapper_gpxsegment')

        # Deleting model 'GPXPoint'
        db.delete_table('webmapper_gpxpoint')

        # Deleting model 'GeoPhoto'
        db.delete_table('webmapper_geophoto')

        # Deleting model 'AerialBoundary'
        db.delete_table('webmapper_aerialboundary')


    models = {
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'webmapper.aerialboundary': {
            'Meta': {'object_name': 'AerialBoundary'},
            'bounds': ('django.contrib.gis.db.models.fields.PolygonField', [], {}),
            'geophoto': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['webmapper.GeoPhoto']", 'unique': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'webmapper.djangomodelgeoitemedge': {
            'Meta': {'object_name': 'DjangoModelGeoItemEdge'},
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'geo_content_type': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'geo_edges'", 'to': "orm['contenttypes.ContentType']"}),
            'geo_object_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        'webmapper.geophoto': {
            'Meta': {'ordering': "['time']", 'object_name': 'GeoPhoto'},
            'content_type_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'gpx': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['webmapper.GPX']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'offset_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'photo_field': ('django.db.models.fields.CharField', [], {'max_length': '1024'}),
            'point': ('django.contrib.gis.db.models.fields.PointField', [], {'null': 'True', 'blank': 'True'}),
            'time': ('django.db.models.fields.DateTimeField', [], {})
        },
        'webmapper.googlemapoptionset': {
            'Meta': {'object_name': 'GoogleMapOptionSet'},
            'disable_default_ui': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'disable_double_click_zoom': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'draggable': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'map_pane': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'google_map_options'", 'unique': 'True', 'to': "orm['webmapper.MapPane']"}),
            'map_type': ('django.db.models.fields.CharField', [], {'default': "'TERRAIN'", 'max_length': '16'}),
            'map_type_control': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'no_clear': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'scale_control': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'scrollwheel': ('django.db.models.fields.BooleanField', [], {'default': 'True'})
        },
        'webmapper.gpx': {
            'Meta': {'object_name': 'GPX'},
            'gpx_file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'webmapper.gpxpoint': {
            'Meta': {'ordering': "['time']", 'object_name': 'GPXPoint'},
            'gpx': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['webmapper.GPX']"}),
            'gpx_segment': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['webmapper.GPXSegment']", 'null': 'True', 'blank': 'True'}),
            'gpx_track': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['webmapper.GPXTrack']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'point': ('django.contrib.gis.db.models.fields.PointField', [], {}),
            'time': ('django.db.models.fields.DateTimeField', [], {})
        },
        'webmapper.gpxsegment': {
            'Meta': {'object_name': 'GPXSegment'},
            'gpx': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['webmapper.GPX']"}),
            'gpx_track': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['webmapper.GPXTrack']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'segment': ('django.contrib.gis.db.models.fields.LineStringField', [], {})
        },
        'webmapper.gpxtrack': {
            'Meta': {'object_name': 'GPXTrack'},
            'gpx': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['webmapper.GPX']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '1024', 'blank': 'True'}),
            'track': ('django.contrib.gis.db.models.fields.MultiLineStringField', [], {})
        },
        'webmapper.kmllayer': {
            'Meta': {'object_name': 'KMLLayer'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'kml': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'kml_url': ('django.db.models.fields.CharField', [], {'max_length': '2048', 'null': 'True', 'blank': 'True'}),
            'map': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'kml_layers'", 'to': "orm['webmapper.Map']"}),
            'revision': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        'webmapper.layer': {
            'Meta': {'object_name': 'Layer'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'map': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'layers'", 'to': "orm['webmapper.Map']"}),
            'revision': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        'webmapper.map': {
            'Meta': {'object_name': 'Map'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'slug': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '128'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        'webmapper.mappane': {
            'Meta': {'object_name': 'MapPane'},
            'center': ('django.contrib.gis.db.models.fields.PointField', [], {'default': "'POINT(-103.852729 44.674512)'"}),
            'default': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'height': ('django.db.models.fields.PositiveIntegerField', [], {'default': '400'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'kml_layers': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'map_panes_kml_layers'", 'symmetrical': 'False', 'to': "orm['webmapper.KMLLayer']"}),
            'layers': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'map_panes_layers'", 'symmetrical': 'False', 'to': "orm['webmapper.Layer']"}),
            'map': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'map_panes'", 'to': "orm['webmapper.Map']"}),
            'map_key': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'map_type': ('django.db.models.fields.CharField', [], {'default': "'google'", 'max_length': '64'}),
            'slug': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '128'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'width': ('django.db.models.fields.PositiveIntegerField', [], {'default': '600'}),
            'zoom': ('django.db.models.fields.DecimalField', [], {'default': '3', 'max_digits': '2', 'decimal_places': '0'})
        },
        'webmapper.point': {
            'Meta': {'object_name': 'Point'},
            'attributes': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'html_repr': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'label': ('django.db.models.fields.CharField', [], {'max_length': '1024', 'null': 'True', 'blank': 'True'}),
            'layer': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'points'", 'to': "orm['webmapper.Layer']"}),
            'point': ('django.contrib.gis.db.models.fields.PointField', [], {})
        },
        'webmapper.polygon': {
            'Meta': {'object_name': 'Polygon'},
            'attributes': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'html_repr': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'label': ('django.db.models.fields.CharField', [], {'max_length': '1024', 'null': 'True', 'blank': 'True'}),
            'layer': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'polygons'", 'to': "orm['webmapper.Layer']"}),
            'polygon': ('django.contrib.gis.db.models.fields.PolygonField', [], {})
        },
        'webmapper.polyline': {
            'Meta': {'object_name': 'PolyLine'},
            'attributes': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'html_repr': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'label': ('django.db.models.fields.CharField', [], {'max_length': '1024', 'null': 'True', 'blank': 'True'}),
            'layer': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'polylines'", 'to': "orm['webmapper.Layer']"}),
            'poly_line': ('django.contrib.gis.db.models.fields.LineStringField', [], {})
        }
    }

    complete_apps = ['webmapper']
