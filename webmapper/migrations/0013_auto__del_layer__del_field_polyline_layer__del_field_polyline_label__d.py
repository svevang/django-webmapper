# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Deleting model 'Layer'
        db.delete_table('webmapper_layer')

        # Removing M2M table for field layers on 'MapPane'
        db.delete_table('webmapper_mappane_layers')

        # Deleting field 'PolyLine.layer'
        db.delete_column('webmapper_polyline', 'layer_id')

        # Deleting field 'PolyLine.label'
        db.delete_column('webmapper_polyline', 'label')

        # Deleting field 'PolyLine.html_repr'
        db.delete_column('webmapper_polyline', 'html_repr')

        # Deleting field 'PolyLine.attributes'
        db.delete_column('webmapper_polyline', 'attributes')

        # Adding field 'PolyLine.map'
        db.add_column('webmapper_polyline', 'map', self.gf('django.db.models.fields.related.ForeignKey')(default=-1, related_name='polylines', to=orm['webmapper.Map']), keep_default=False)

        # Deleting field 'Polygon.layer'
        db.delete_column('webmapper_polygon', 'layer_id')

        # Deleting field 'Polygon.label'
        db.delete_column('webmapper_polygon', 'label')

        # Deleting field 'Polygon.html_repr'
        db.delete_column('webmapper_polygon', 'html_repr')

        # Deleting field 'Polygon.attributes'
        db.delete_column('webmapper_polygon', 'attributes')

        # Adding field 'Polygon.map'
        db.add_column('webmapper_polygon', 'map', self.gf('django.db.models.fields.related.ForeignKey')(default=-1, related_name='polygons', to=orm['webmapper.Map']), keep_default=False)

        # Deleting field 'Point.layer'
        db.delete_column('webmapper_point', 'layer_id')

        # Deleting field 'Point.label'
        db.delete_column('webmapper_point', 'label')

        # Deleting field 'Point.html_repr'
        db.delete_column('webmapper_point', 'html_repr')

        # Deleting field 'Point.attributes'
        db.delete_column('webmapper_point', 'attributes')

        # Adding field 'Point.map'
        db.add_column('webmapper_point', 'map', self.gf('django.db.models.fields.related.ForeignKey')(default=-1, related_name='points', to=orm['webmapper.Map']), keep_default=False)


    def backwards(self, orm):
        
        # Adding model 'Layer'
        db.create_table('webmapper_layer', (
            ('map', self.gf('django.db.models.fields.related.ForeignKey')(related_name='layers', to=orm['webmapper.Map'])),
            ('description', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('revision', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
        ))
        db.send_create_signal('webmapper', ['Layer'])

        # Adding M2M table for field layers on 'MapPane'
        db.create_table('webmapper_mappane_layers', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('mappane', models.ForeignKey(orm['webmapper.mappane'], null=False)),
            ('layer', models.ForeignKey(orm['webmapper.layer'], null=False))
        ))
        db.create_unique('webmapper_mappane_layers', ['mappane_id', 'layer_id'])

        # Adding field 'PolyLine.layer'
        db.add_column('webmapper_polyline', 'layer', self.gf('django.db.models.fields.related.ForeignKey')(default=-1, related_name='polylines', to=orm['webmapper.Layer']), keep_default=False)

        # Adding field 'PolyLine.label'
        db.add_column('webmapper_polyline', 'label', self.gf('django.db.models.fields.CharField')(max_length=1024, null=True, blank=True), keep_default=False)

        # Adding field 'PolyLine.html_repr'
        db.add_column('webmapper_polyline', 'html_repr', self.gf('django.db.models.fields.TextField')(null=True, blank=True), keep_default=False)

        # Adding field 'PolyLine.attributes'
        db.add_column('webmapper_polyline', 'attributes', self.gf('django.db.models.fields.TextField')(null=True, blank=True), keep_default=False)

        # Deleting field 'PolyLine.map'
        db.delete_column('webmapper_polyline', 'map_id')

        # Adding field 'Polygon.layer'
        db.add_column('webmapper_polygon', 'layer', self.gf('django.db.models.fields.related.ForeignKey')(default=-1, related_name='polygons', to=orm['webmapper.Layer']), keep_default=False)

        # Adding field 'Polygon.label'
        db.add_column('webmapper_polygon', 'label', self.gf('django.db.models.fields.CharField')(max_length=1024, null=True, blank=True), keep_default=False)

        # Adding field 'Polygon.html_repr'
        db.add_column('webmapper_polygon', 'html_repr', self.gf('django.db.models.fields.TextField')(null=True, blank=True), keep_default=False)

        # Adding field 'Polygon.attributes'
        db.add_column('webmapper_polygon', 'attributes', self.gf('django.db.models.fields.TextField')(null=True, blank=True), keep_default=False)

        # Deleting field 'Polygon.map'
        db.delete_column('webmapper_polygon', 'map_id')

        # Adding field 'Point.layer'
        db.add_column('webmapper_point', 'layer', self.gf('django.db.models.fields.related.ForeignKey')(default=-1, related_name='points', to=orm['webmapper.Layer']), keep_default=False)

        # Adding field 'Point.label'
        db.add_column('webmapper_point', 'label', self.gf('django.db.models.fields.CharField')(max_length=1024, null=True, blank=True), keep_default=False)

        # Adding field 'Point.html_repr'
        db.add_column('webmapper_point', 'html_repr', self.gf('django.db.models.fields.TextField')(null=True, blank=True), keep_default=False)

        # Adding field 'Point.attributes'
        db.add_column('webmapper_point', 'attributes', self.gf('django.db.models.fields.TextField')(null=True, blank=True), keep_default=False)

        # Deleting field 'Point.map'
        db.delete_column('webmapper_point', 'map_id')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'webmapper.aerialboundary': {
            'Meta': {'object_name': 'AerialBoundary'},
            'bounds': ('django.contrib.gis.db.models.fields.PolygonField', [], {}),
            'geophoto': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['webmapper.GeoPhoto']", 'unique': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'webmapper.djangomodelgeoitemedge': {
            'Meta': {'object_name': 'DjangoModelGeoItemEdge'},
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'geo_content_type': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'geo_edges'", 'to': "orm['contenttypes.ContentType']"}),
            'geo_object_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        'webmapper.geophoto': {
            'Meta': {'ordering': "['time']", 'object_name': 'GeoPhoto'},
            'content_type_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'gpx': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['webmapper.GPX']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'offset_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'photo_field': ('django.db.models.fields.CharField', [], {'max_length': '1024'}),
            'point': ('django.contrib.gis.db.models.fields.PointField', [], {'null': 'True', 'blank': 'True'}),
            'time': ('django.db.models.fields.DateTimeField', [], {})
        },
        'webmapper.googlemapoptionset': {
            'Meta': {'object_name': 'GoogleMapOptionSet'},
            'disableDefaultUI': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'disableDoubleClickZoom': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'draggable': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mapTypeControl': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'mapTypeId': ('django.db.models.fields.CharField', [], {'default': "'terrain'", 'max_length': '16'}),
            'map_pane': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'google_options'", 'unique': 'True', 'to': "orm['webmapper.MapPane']"}),
            'noClear': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'scaleControl': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'scrollwheel': ('django.db.models.fields.BooleanField', [], {'default': 'True'})
        },
        'webmapper.gpx': {
            'Meta': {'object_name': 'GPX'},
            'gpx_file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'webmapper.gpxpoint': {
            'Meta': {'ordering': "['time']", 'object_name': 'GPXPoint'},
            'gpx': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['webmapper.GPX']"}),
            'gpx_segment': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['webmapper.GPXSegment']", 'null': 'True', 'blank': 'True'}),
            'gpx_track': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['webmapper.GPXTrack']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'point': ('django.contrib.gis.db.models.fields.PointField', [], {}),
            'time': ('django.db.models.fields.DateTimeField', [], {})
        },
        'webmapper.gpxsegment': {
            'Meta': {'object_name': 'GPXSegment'},
            'gpx': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['webmapper.GPX']"}),
            'gpx_track': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['webmapper.GPXTrack']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'segment': ('django.contrib.gis.db.models.fields.LineStringField', [], {})
        },
        'webmapper.gpxtrack': {
            'Meta': {'object_name': 'GPXTrack'},
            'gpx': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['webmapper.GPX']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '1024', 'blank': 'True'}),
            'track': ('django.contrib.gis.db.models.fields.MultiLineStringField', [], {})
        },
        'webmapper.kmllayer': {
            'Meta': {'object_name': 'KMLLayer'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'kml': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'kml_url': ('django.db.models.fields.CharField', [], {'max_length': '2048', 'null': 'True', 'blank': 'True'}),
            'map': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'kml_layers'", 'to': "orm['webmapper.Map']"}),
            'revision': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        'webmapper.map': {
            'Meta': {'object_name': 'Map'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'map_pane': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'map'", 'unique': 'True', 'to': "orm['webmapper.MapPane']"}),
            'slug': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']", 'null': 'True', 'blank': 'True'})
        },
        'webmapper.mappane': {
            'Meta': {'object_name': 'MapPane'},
            'center': ('django.contrib.gis.db.models.fields.PointField', [], {'default': "'POINT(-103.852729 44.674512)'"}),
            'height': ('django.db.models.fields.PositiveIntegerField', [], {'default': '400'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'kml_layers': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'map_panes_kml_layers'", 'symmetrical': 'False', 'to': "orm['webmapper.KMLLayer']"}),
            'map_key': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'map_type': ('django.db.models.fields.CharField', [], {'default': "'google'", 'max_length': '64'}),
            'width': ('django.db.models.fields.PositiveIntegerField', [], {'default': '600'}),
            'zoom': ('django.db.models.fields.DecimalField', [], {'default': '3', 'max_digits': '2', 'decimal_places': '0'})
        },
        'webmapper.openlayersoptionset': {
            'Meta': {'object_name': 'OpenLayersOptionSet'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'map_pane': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'openlayers_map_options'", 'unique': 'True', 'to': "orm['webmapper.MapPane']"}),
            'map_type': ('django.db.models.fields.CharField', [], {'default': "'terrain'", 'max_length': '64'}),
            'num_zoom_levels': ('django.db.models.fields.PositiveIntegerField', [], {'default': '20'})
        },
        'webmapper.point': {
            'Meta': {'object_name': 'Point'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'map': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'points'", 'to': "orm['webmapper.Map']"}),
            'point': ('django.contrib.gis.db.models.fields.PointField', [], {})
        },
        'webmapper.polygon': {
            'Meta': {'object_name': 'Polygon'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'map': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'polygons'", 'to': "orm['webmapper.Map']"}),
            'polygon': ('django.contrib.gis.db.models.fields.PolygonField', [], {})
        },
        'webmapper.polyline': {
            'Meta': {'object_name': 'PolyLine'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'map': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'polylines'", 'to': "orm['webmapper.Map']"}),
            'poly_line': ('django.contrib.gis.db.models.fields.LineStringField', [], {})
        }
    }

    complete_apps = ['webmapper']
