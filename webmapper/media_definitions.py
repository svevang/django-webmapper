import os
from django.core import exceptions
from django.core.urlresolvers import reverse
from django.utils.functional import lazy

from webmapper.util import webmapper_eager_static_path
from webmapper.util import webmapper_static_path
from webmapper.util import webmapper_path
from webmapper.util import get_setting

def lazy_list(lst):
  return lazy( lambda: map(lambda x:x+"", lst), list)()

# CSS
css = [
    webmapper_static_path('css/webmapper_style.css'),
    webmapper_static_path('lib/jquery-ui-1.8.20/css/webmapper/jquery-ui-1.8.20.custom.css'),
    webmapper_static_path('lib/jquery_fileupload/jquery.fileupload-ui.css'),
    ]

# JS
libs_js = [get_setting("JQUERY_URL"),
            webmapper_static_path('lib/jquery-ui-1.8.20/js/jquery-ui-1.8.20.custom.min.js'),
            webmapper_static_path('lib/jquery_fileupload/jquery.iframe-transport.js'),
            webmapper_static_path('lib/jquery_fileupload/jquery.fileupload.js'),
            webmapper_static_path('lib/jquery_fileupload/jquery.fileupload-fp.js'),
            webmapper_static_path('lib/jquery_fileupload/jquery.fileupload-ui.js'),
            webmapper_static_path('lib/jquery_fileupload/jquery.fileupload-jui.js'),
            webmapper_static_path('lib/json2.js'),
            webmapper_static_path('lib/backbone/underscore.js'),
            webmapper_static_path('lib/backbone/backbone.js')]

external_map_js = [
           'http://maps.googleapis.com/maps/api/js?sensor=false',
           webmapper_static_path('lib/openlayers/2.11/OpenLayers.js'),
         ]

webmapper_constants_js = [webmapper_path('webmapper_constants')]

webmapper_map_js = [
           webmapper_static_path('core.js'),
           webmapper_static_path('geo.js'),
           webmapper_static_path('google.js'),
           webmapper_static_path('openlayers.js'),
           webmapper_static_path('application_views.js'),
           webmapper_static_path('application_tools.js'),
           webmapper_static_path('events.js'),
         ] 

map_js = external_map_js + webmapper_constants_js + webmapper_map_js

js = libs_js + map_js

WEBMAPPER_MEDIA = { 'js':                    lazy_list(js),
                    'css':                   lazy_list(css),
                    'libs_js':               lazy_list(libs_js),
                    'external_map_js':       lazy_list(external_map_js),
                    'webmapper_constants_js':lazy_list(webmapper_constants_js),
                    'webmapper_map_js':      lazy_list(webmapper_map_js),
                    'map_js':                lazy_list(map_js),
                  }
