class NotRegistered(Exception):
  pass

class ClassRegistry(dict):

  def __getitem__(self, key):
    if self.has_key(key):
      return super(ClassRegistry,self).get(key)
    else:
      return self._lookup_baseclass(key)

  def get(self, key, default=None):
    if self.has_key(key):
      return super(ClassRegistry,self).get(key)
    else:
      try:
        return self._lookup_baseclass(key)
      except NotRegistered:
        return default

  def _lookup_baseclass(self, key):
    #TODO sort classes by possible heirarchies
    # and traverse that tree upwards towards ancestors
    for klass in self.keys():
      if issubclass(key, klass):
        return super(ClassRegistry,self).__getitem__(klass)
    raise NotRegistered()
  

model_view_registry = ClassRegistry()
