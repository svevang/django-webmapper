.. Webmapper documentation master file, created by
   sphinx-quickstart on Mon Jan 31 13:44:37 2011.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

What is Webmapper
=================

Webmapper is a tool to quickly create and manipulate Google_ and OpenLayers_
maps. It allows you to manage KML and more complex geographic attributes. 
Webmapper is built on Geodjango and PostGIS.

.. _google: https://code.google.com/apis/maps/documentation/javascript/reference.html
.. _openlayers: http://dev.openlayers.org/releases/OpenLayers-2.10/doc/apidocs/files/OpenLayers-js.html

API
===

Webmapper is built on django-piston. The goal here is a pluggable auth system
where you write a simple function to determine if a user has access to a map.
The simplest case is to override the default `CookieAuthentication` class in 
`webmapper.api.auth`.

Tests:
=====

Webmapper uses django-nose_ to run unittest. Follow the installation
instructions and run `./manage.py test`

.. _django-nose: https://github.com/jbalogh/django-nose

tool palette
============

implement custom tools that tie into the django datamodel
Contents:

.. toctree::
   :maxdepth: 2
   intro

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

