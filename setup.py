import os
import sys
from setuptools import setup, find_packages

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()
    
setup(
    name = "django-webmapper",
    version = "0.0.2alpha",
    description='GIS Lite on Google and Openlayers Maps.',
    long_description = read('README.rst'),
    url='https://bitbucket.org/svevang/django-webmapper',
    #download_url='',
    license = 'BSD',
    author = 'Sam Vevang',
    author_email = 'sam.vevang@gmail.com',
    packages = find_packages(),
    classifiers = [
        'Environment :: Console',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'Intended Audience :: Information Technology',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
    ],
    dependency_links = ['https://bitbucket.org/jespern/django-piston/get/tip.zip#egg=django-piston',
                        'https://github.com/lincolnloop/django-ttag/zipball/master#egg=django-ttag',
                       ],
    install_requires = ['django >= 1.3', 
                        'psycopg2', 
                        'simplejson', 
                        'lxml', 
                        'python-dateutil', 
                        'django-piston',
                        'django-nose',
                        'webassets',
                        'django-ttag'],
)
