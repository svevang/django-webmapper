Webmapper is a Heavy-Duty Mapping Infrastructure
================================================

Any geographic datamodel that you can express in geodjango can also be 
expressed as webmapper map. The system is designed for ultimate flexibility, 
allowing you precise control over back-end state and client-side functionality.

Webmapper is geodjango_ and PostGIS_.

.. _geodjango: http://geodjango.org/
.. _PostGIS: http://postgis.refractions.net/

Installation
============

1) Install webmapper code and dependencies
::

  python setup.py install


add ``webmapper`` to your ``settings.INSTALLED_APPS``
::

  INSTALLED_APPS =+ ('webmapper')

You must have ``contrib.sites``, and ``contrib.staticfiles`` in your installed apps as well.

QuickStart
==========

Bootstrap your template with WEBMAPPER_MEDIA. 
::

    {% include 'webmapper/_webmapper_media.html' %}

Next, invoke the Map creation
::

    <script type="text/javascript">
    $(document).ready(function(){
      window.wmap = new webmapper.Map({id:{{map.id}}, 
                                       domId:"webmapper-container"})
    })
    </script>


The Map invocation is designed to happen in a django template. Here we pass in 
the ``webmapper.models.Map`` object

It is enough to pass in a model ``id`` and a base
DOM element id. 

Features
========

 * Bind any geographic datamodel to a client-side map. 
 * Authenticate users and define custom permissions. e.g:
   - Display certain maps only for users that are logged in 
   - Map features (like points and polygons) can be tied to actions and users
 
Tests:
=====

Webmapper uses django-nose_ to run unittest. Follow the installation
instructions and run ``./manage.py test webmapper``

.. _django-nose: https://github.com/jbalogh/django-nose

Example
============

Check out webmapper.org_ as a working example of what a webmapper map can do!

.. _webmapper.org: http://www.webmapper.org


Note
====

If you are using postgresql 9.1 and django 1.3.* you must set 
`standard_conforming_strings = off`
in your `/etc/postgresql/9.1/main/postgresql.conf` file


.. toctree::
   :maxdepth: 2
   intro

